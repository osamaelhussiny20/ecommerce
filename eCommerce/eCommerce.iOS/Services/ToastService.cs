﻿using CoreGraphics;
using Foundation;
using eCommerce.Enums;
using eCommerce.Services.UserDialog;
using eCommerce.iOS.Services;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Foundation;
using UIKit;

[assembly: Dependency(typeof(ToastService))]
namespace eCommerce.iOS.Services
{
    public class ToastService : IToastService
    {
        private const int ToastHeight = 50;
        private const int ToastMargin = 20;

        private UIWindow _window;
        private UIView _toastView;
        private UIView _overlayView;
        private UIView _statusBar;
        private UILabel _toastlabel;
        private Color _backgroundColor;
        private ToastType _toastType;
        private bool _isShowing;
        private bool _iosEleven => UIDevice.CurrentDevice.CheckSystemVersion(11, 0);

        public void ShowToast(string message, ToastType toastType, int duration = 5, int fontSize = 14, Color? backgroundColor = null, Color? textColor = null)
        {
            _toastType = toastType;

            if (toastType == ToastType.Alert)
            {
                _backgroundColor = Color.FromHex("#C72B41");
            }
            else
            {
                _backgroundColor = Color.FromHex("#00A651");
            }

            if (!_isShowing)
            {
                _isShowing = true;

                _window = UIApplication.SharedApplication.KeyWindow;
                _statusBar = UIApplication.SharedApplication.ValueForKey(new NSString("statusBar")) as UIView;

                BuildLayout(message, fontSize, backgroundColor, textColor);
                AnimateToast(duration);
            }
        }

        private void BuildLayout(string message, int fontSize, Color? backgroundColor, Color? textColor)
        {
            var convertedBackgroundColor = (backgroundColor ?? _backgroundColor).ToUIColor();
            _toastView = new UIView(new CGRect(0, -ToastHeight, _window.Frame.Width, ToastHeight))
            {
                BackgroundColor = convertedBackgroundColor,
                LayoutMargins = new UIEdgeInsets(ToastMargin, 0, ToastMargin, 0),
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            var convertedTextColor = (textColor ?? Color.White).ToUIColor();
            _toastlabel = new UILabel(new CGRect(0, 0, _toastView.Frame.Size.Width, _toastView.Frame.Size.Height))
            {
                Text = message,
                TextAlignment = UITextAlignment.Center,
                Font = UIFont.SystemFontOfSize(fontSize),
                Lines = 0,
                TextColor = convertedTextColor,
                TranslatesAutoresizingMaskIntoConstraints = false
            };

            _overlayView = new UIView(new CGRect(0, -_statusBar.Frame.Height, _window.Frame.Width, _statusBar.Frame.Height))
            {
                BackgroundColor = convertedBackgroundColor
            };

            _toastView.AddSubview(_toastlabel);

            if (_toastType == ToastType.Alert)
            {
                if (_iosEleven)
                {
                    _window.AddSubview(_overlayView);
                }

                _window.AddSubview(_toastView);
                UpdateConstraits(_window);
            }
            else
            {
                _toastView.LayoutMargins = new UIEdgeInsets(10, 0, 10, 0);

                if (_iosEleven)
                {
                    _statusBar.Superview.AddSubview(_overlayView);
                }

                _statusBar.Superview.AddSubview(_toastView);
                _statusBar.Superview.BringSubviewToFront(_statusBar);

                UpdateConstraits(_statusBar.Superview);
            }
        }

        private void UpdateConstraits<T>(T rootView) where T : UIView
        {
            var heightModifier = _iosEleven ? _window.SafeAreaInsets.Top : 0;

            NSLayoutConstraint.Create(_toastView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, rootView, NSLayoutAttribute.Top, 1.0f, 0.0f + heightModifier).Active = true;
            NSLayoutConstraint.Create(_toastView, NSLayoutAttribute.BottomMargin, NSLayoutRelation.Equal, _toastlabel, NSLayoutAttribute.BottomMargin, 1.0f, 0.0f).Active = true;
            NSLayoutConstraint.Create(_toastView, NSLayoutAttribute.TopMargin, NSLayoutRelation.Equal, rootView, NSLayoutAttribute.TopMargin, 1.0f, 0.0f).Active = true;

            NSLayoutConstraint.Create(_toastView, NSLayoutAttribute.Leading, NSLayoutRelation.Equal, rootView, NSLayoutAttribute.Leading, 1.0f, 0.0f).Active = true;
            NSLayoutConstraint.Create(_toastView, NSLayoutAttribute.Trailing, NSLayoutRelation.Equal, rootView, NSLayoutAttribute.Trailing, 1.0f, 0.0f).Active = true;

            NSLayoutConstraint.Create(_toastlabel, NSLayoutAttribute.Leading, NSLayoutRelation.Equal, _toastView, NSLayoutAttribute.Leading, 1.0f, 0.0f).Active = true;
            NSLayoutConstraint.Create(_toastlabel, NSLayoutAttribute.Trailing, NSLayoutRelation.Equal, _toastView, NSLayoutAttribute.Trailing, 1.0f, 0.0f).Active = true;
            NSLayoutConstraint.Create(_toastlabel, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, _toastView, NSLayoutAttribute.Bottom, 1.0f, 0.0f).Active = true;
            NSLayoutConstraint.Create(_toastlabel, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, _toastView, NSLayoutAttribute.CenterY, 1.0f, 0.0f).Active = true;
        }

        private void AnimateToast(int duration)
        {
            var animationDuration = duration / 10f;
            var delayDuration = duration / 2.5f;

            if (_toastType == ToastType.Alert)
            {
                AnimateAlert(animationDuration, delayDuration);
            }
            else
            {
                AnimateNotification(animationDuration, delayDuration);
            }
        }

        private void AnimateAlert(float animationDuration, float delayDuration)
        {
            UIView.Animate(animationDuration, 0, UIViewAnimationOptions.CurveEaseOut, () =>
            {
                _window.WindowLevel = UIWindowLevel.StatusBar + 1;
                _toastView.Frame = new CGRect(0, 0, _toastView.Frame.Width, _toastView.Frame.Height);
                _overlayView.Frame = new CGRect(0, 0, _overlayView.Frame.Width, _overlayView.Frame.Height);
            }, () =>
            {
                UIView.Animate(animationDuration, delayDuration, UIViewAnimationOptions.CurveEaseOut, () =>
                {
                    _toastView.Frame = new CGRect(0, -_toastView.Frame.Height, _toastView.Frame.Width, _toastView.Frame.Height);
                    _overlayView.Frame = new CGRect(0, -_overlayView.Frame.Height, _overlayView.Frame.Width, _overlayView.Frame.Height);

                }, () =>
                {
                    _overlayView.RemoveFromSuperview();
                    _window.WindowLevel = UIWindowLevel.Normal;
                    _isShowing = false;
                    _toastView.RemoveFromSuperview();
                });
            });
        }

        private void AnimateNotification(float animationDuration, float delayDuration)
        {
            UIView.Animate(animationDuration, 0, UIViewAnimationOptions.CurveEaseOut, () =>
            {
                _toastView.Frame = new CGRect(0, 0, _toastView.Frame.Width, _toastView.Frame.Height);
                if (_iosEleven)
                {
                    _overlayView.Frame = new CGRect(0, 0, _overlayView.Frame.Width, _overlayView.Frame.Height);
                }

                _window.Frame = new CGRect(_window.Frame.X, _window.Frame.Y + _statusBar.Frame.Height, _window.Frame.Width, _window.Frame.Height - _statusBar.Frame.Height);
            }, null);
        }

        private void NotificationDismissAnimation(float animationDuration)
        {
            UIView.Animate(animationDuration, 0, UIViewAnimationOptions.CurveEaseOut, () =>
            {
                _toastView.Frame = new CGRect(0, -_toastView.Frame.Height, _toastView.Frame.Width, _toastView.Frame.Height);

                if (_iosEleven)
                {
                    _overlayView.Frame = new CGRect(0, -_statusBar.Frame.Height, _overlayView.Frame.Width, _overlayView.Frame.Height);
                }
                _window.Frame = new CGRect(_window.Frame.X, 0, _window.Frame.Width, _window.Frame.Height + _statusBar.Frame.Height);
            }, () =>
            {
                _isShowing = false;
                _toastView.RemoveFromSuperview();
            });
        }

        public void DismissNotification(int duration = 5)
        {
            var animationDuration = duration / 10f;
            NotificationDismissAnimation(animationDuration);
        }
    }

}