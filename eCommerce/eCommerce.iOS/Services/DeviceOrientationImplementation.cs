﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using eCommerce.iOS.Services;
using eCommerce.Services;
using UIKit;
using Xamarin.Forms;

[assembly:Dependency(typeof (DeviceOrientationImplementation))]
namespace eCommerce.iOS.Services
{
    public class DeviceOrientationImplementation : IDeviceOrientation 
    {
        public DeviceOrientationImplementation() { }
        public DeviceOrientations GetOrientation()
        {
            var currentOrientation = UIApplication.SharedApplication.StatusBarOrientation;
            bool isPortrait = currentOrientation == UIInterfaceOrientation.Portrait || currentOrientation == UIInterfaceOrientation.PortraitUpsideDown;
            return isPortrait ? DeviceOrientations.Portrait : DeviceOrientations.Landscape;
        }

        public void SetLandscape()
        {
            UIDevice.CurrentDevice.SetValueForKey(new NSNumber((int)UIInterfaceOrientation.LandscapeRight), new NSString("orientation"));
        }


        public void SetPortrait()
        {
           UIDevice.CurrentDevice.SetValueForKey(new NSNumber((int)UIInterfaceOrientation.Portrait), new NSString("orientation"));
        }

    }
}