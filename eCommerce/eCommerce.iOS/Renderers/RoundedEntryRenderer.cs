﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreGraphics;
using Foundation;
using eCommerce.Controls;
using eCommerce.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
[assembly: ExportRenderer(typeof(RoundedEntry), typeof(RoundedEntryRenderer))]
namespace eCommerce.iOS.Renderers
{
    public class RoundedEntryRenderer: EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                var roundedView = Element as RoundedEntry;

                if (roundedView != null)
                {
                    NativeView.Layer.CornerRadius = roundedView.BorderRadius;
                    NativeView.Layer.BorderWidth = roundedView.BorderThickness;
                    NativeView.Layer.BorderColor = roundedView.BorderColor.ToCGColor();
                    NativeView.Layer.BackgroundColor = roundedView.EntryBackgroundColor.ToCGColor();
                }

                if (roundedView != null && !roundedView.DisplaySuggestions)
                {
                    Control.AutocorrectionType = UITextAutocorrectionType.No;

                    SetReturnType(roundedView);

                    Control.ShouldReturn += (UITextField tf) =>
                    {
                        roundedView.InvokeCompleted();
                        return true;
                    };
                }

                Control.LeftView = new UIView(new CGRect(0, 0, 15, Control.Frame.Height));
                Control.RightView = new UIView(new CGRect(0, 0, 15, Control.Frame.Height));
                Control.LeftViewMode = UITextFieldViewMode.Always;
                Control.RightViewMode = UITextFieldViewMode.Always;
                Control.BorderStyle = UITextBorderStyle.None;
            }
        }


        private void SetReturnType(RoundedEntry entry)
        {
            ReturnType type = entry.ReturnType;

            switch (type)
            {
                case ReturnType.Go:
                    Control.ReturnKeyType = UIReturnKeyType.Go;
                    break;
                case ReturnType.Next:
                    Control.ReturnKeyType = UIReturnKeyType.Next;
                    break;
                case ReturnType.Send:
                    Control.ReturnKeyType = UIReturnKeyType.Send;
                    break;
                case ReturnType.Search:
                    Control.ReturnKeyType = UIReturnKeyType.Search;
                    break;
                case ReturnType.Done:
                    Control.ReturnKeyType = UIReturnKeyType.Done;
                    break;
                default:
                    Control.ReturnKeyType = UIReturnKeyType.Default;
                    break;
            }
        }
    }

}