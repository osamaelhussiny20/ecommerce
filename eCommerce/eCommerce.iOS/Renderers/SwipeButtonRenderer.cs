﻿using System.Threading.Tasks;
using eCommerce.Controls;
using eCommerce.iOS.Control;
using eCommerce.iOS.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(SwipeButton), typeof(SwipeButtonRenderer))]
namespace eCommerce.iOS.Renderers
{
    public class SwipeButtonRenderer : ViewRenderer<SwipeButton, NativeSwipeButton>
    {
        private SwipeButton _swipeButton;
        private NativeSwipeButton _nativeSwipeButton;

        protected override void OnElementChanged(ElementChangedEventArgs<SwipeButton> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
            {
                _swipeButton = Element as SwipeButton;

                _nativeSwipeButton = new NativeSwipeButton(_swipeButton.DragButtonSize, _swipeButton.Text, _swipeButton.Description, _swipeButton.IconSource,
                    _swipeButton.BackgroundColor.ToUIColor(), _swipeButton.DragButtonBackgroundColor.ToUIColor(), _swipeButton.TextColor.ToUIColor(),
                    _swipeButton.FontFamily, _swipeButton.FontSize, _swipeButton.DescriptionFontSize);

                SetNativeControl(_nativeSwipeButton);

                _nativeSwipeButton.SwipeComplited = SwipeComplited;
            }
        }

        private async void SwipeComplited()
        {
            var swipeCompletionSource = new TaskCompletionSource<bool>();
            _swipeButton.SwipeComplitedCommand?.Execute(swipeCompletionSource);

            var commandComplited = await swipeCompletionSource.Task;

            if (!commandComplited)
            {
                _nativeSwipeButton.ResetButton();
            }
        }
    }
}