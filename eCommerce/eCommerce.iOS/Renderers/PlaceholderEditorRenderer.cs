﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using eCommerce.Controls;
using eCommerce.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
[assembly: ExportRenderer(typeof(PlaceholderEditor), typeof(PlaceholderEditorRenderer))]
namespace eCommerce.iOS.Renderers
{
    public class PlaceholderEditorRenderer : EditorRenderer
    {
        private string Placeholder { get; set; }

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);
            var element = this.Element as PlaceholderEditor;

            if (Control != null && element != null)
            {
                Placeholder = element.Placeholder;
                Control.TextColor = UIColor.LightGray;
                Control.Text = Placeholder;
                Control.Layer.CornerRadius = 15;

                Control.ShouldBeginEditing += (UITextView textView) => {
                    if (textView.Text == Placeholder)
                    {
                        textView.Text = "";
                        textView.TextColor = element.TextColor.ToUIColor(); // Text Color
                    }

                    return true;
                };

                Control.ShouldEndEditing += (UITextView textView) => {
                    if (textView.Text == "")
                    {
                        textView.Text = Placeholder;
                        textView.TextColor = element.PlaceholderColor.ToUIColor(); // Placeholder Color
                    }

                    return true;
                };
            }
        }
    }
}