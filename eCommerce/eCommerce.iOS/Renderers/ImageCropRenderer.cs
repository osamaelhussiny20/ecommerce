﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreGraphics;
using Foundation;
using eCommerce.Controls;
using eCommerce.iOS.Controls;
using eCommerce.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CropImageControl), typeof(ImageCropRenderer))]
namespace eCommerce.iOS.Renderers
{
    public class ImageCropRenderer : ViewRenderer<CropImageControl, CropImageLayout>
    {
        private CropImageControl _cropImageControl;
        private CropImageLayout _cropImageLayout;

        protected override void OnElementChanged(ElementChangedEventArgs<CropImageControl> e)
        {
            base.OnElementChanged(e);

            if (Element != null)
            {
                _cropImageControl = Element as CropImageControl;

                var frame = new CGRect(0, 0, Frame.Width, Frame.Height);
                _cropImageLayout = new CropImageLayout(Frame, _cropImageControl.ImageSource);
                _cropImageControl.ImageSourceAddedCommand = new Command(ImageSourceAddedExecute);
                _cropImageControl.GetCroppedImageCommand = new Command(GetCroppedImageCommandExecute);
                SetNativeControl(_cropImageLayout);
            }
        }

        private void GetCroppedImageCommandExecute(object obj)
        {
            var data = _cropImageLayout.GetCroppedImage();
            _cropImageControl.ImageCroppedCommand?.Execute(data);
        }

        private void ImageSourceAddedExecute(object obj)
        {
            var param = obj as string;
            if (!string.IsNullOrEmpty(param))
            {
                _cropImageLayout.LoadImage(param);
            }
        }
    }
}