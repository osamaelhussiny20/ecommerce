﻿using System.Threading.Tasks;
using BaseMvvmToolkit.Pages;
using BaseMvvmToolkit.ViewModels;
using eCommerce.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(BaseNavigationPage), typeof(BaseNavigationPageRenderer))]
namespace eCommerce.iOS.Renderers
{
    public class BaseNavigationPageRenderer : NavigationRenderer
    {
        private BasePage _basePage;

        protected override Task<bool> OnPushAsync(Page view, bool animated)
        {
            var defaultValue = base.OnPushAsync(view, animated);
            var bindingContext = view.BindingContext as BaseViewModel;

            string imagePath = string.Empty;

            if (view is BasePage basePage)
            {
                imagePath = basePage.BackButtonIcon;
                _basePage = basePage;
            }

            if (view is MasterDetailPage masterDetailPage)
            {
                _basePage = masterDetailPage.Detail as BasePage;
                imagePath = _basePage.BackButtonIcon;
            }

            var backButtonImage = new UIBarButtonItem(
                UIImage.FromBundle(imagePath),
                UIBarButtonItemStyle.Plain,
                (sender, args) =>
                {
                    HandleNavigationClick(_basePage);
                });

            if (bindingContext?.DrawerMenuViewModelType == null)
            {
                TopViewController.NavigationItem.LeftBarButtonItem = backButtonImage;
            }

            return defaultValue;
        }

        private void HandleNavigationClick(BasePage basePage)
        {
            if (basePage != null)
            {
                if (basePage.BackButtonCommand != null && basePage.BackButtonCommand.CanExecute(null))
                {
                    basePage.BackButtonCommand.Execute(null);
                }
                else
                {
                    TopViewController.NavigationController.PopViewController(true);
                }
            }
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            NavigationBar.SetBackgroundImage(new UIImage(), UIBarMetrics.Default);
            NavigationBar.ShadowImage = new UIImage();
        }
    }
}