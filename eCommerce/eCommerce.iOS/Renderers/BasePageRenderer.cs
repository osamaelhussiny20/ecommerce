﻿using BaseMvvmToolkit.Pages;
using Foundation;
using eCommerce.iOS.Renderers;
using eCommerce.Pages.Menu;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(BasePage), typeof(BasePageRenderer))]
namespace eCommerce.iOS.Renderers
{
    public class BasePageRenderer : PageRenderer
    {
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            var basePage = Element as BasePage;
            var hasNavigationBar = NavigationPage.GetHasNavigationBar(basePage);

            if (Element is MenuMasterPage menuPage)
            {
                SetDefaultPadding(menuPage);
            }

            if (!hasNavigationBar)
            {
                SetDefaultPadding(basePage);
            }
        }

        private static void SetDefaultPadding(BasePage basePage)
        {
            if (!UIDevice.CurrentDevice.CheckSystemVersion(11, 0))
            {
                var statusBar = UIApplication.SharedApplication.ValueForKey(new NSString("statusBar")) as UIView;
                basePage.Padding = new Thickness(basePage.Padding.Left, basePage.Padding.Top + statusBar.Frame.Height, basePage.Padding.Right, basePage.Padding.Bottom);
            }
        }
    }
}