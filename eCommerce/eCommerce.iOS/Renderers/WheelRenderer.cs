﻿using System.ComponentModel;
using Foundation;
using eCommerce.Controls;
using eCommerce.iOS.Control;
using eCommerce.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Wheel), typeof(WheelRenderer))]
namespace eCommerce.iOS.Renderers
{
    public class WheelRenderer : ViewRenderer<Wheel, WheelLayout>
    {
        private Wheel _wheel;

        protected override void OnElementChanged(ElementChangedEventArgs<Wheel> e)
        {
            base.OnElementChanged(e);

            _wheel = Element as Wheel;
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if (e.PropertyName == Wheel.ProfilePictureProperty.PropertyName)
            {
                if (_wheel == null)
                {
                    return;
                }

                var profilePicture = _wheel.ProfilePicture;
                var backgroundImage = _wheel.BackgroundImage;

                if (profilePicture != null && !string.IsNullOrEmpty(backgroundImage))
                {
                    var data = NSData.FromArray(profilePicture);
                    var image = UIImage.LoadFromData(data);

                    var wheelLayout = new WheelLayout(image, backgroundImage, UpdateSpinAmount);

                    var viewContent = wheelLayout.ToView();

                    viewContent.HeightRequest = _wheel.Width;
                    viewContent.WidthRequest = _wheel.Width;

                    _wheel.Content = viewContent;
                }
            }
        }

        private void UpdateSpinAmount(float spinAmount)
        {
            _wheel.SpinAmount = (int)spinAmount;
        }
    }
}
