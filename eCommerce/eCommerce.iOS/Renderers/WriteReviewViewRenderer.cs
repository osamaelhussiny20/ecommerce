﻿using Foundation;
using eCommerce.Controls;
using eCommerce.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(WriteReviewView), typeof(WriteReviewViewRenderer))]
namespace eCommerce.iOS.Renderers
{
    public class WriteReviewViewRenderer : ViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            base.OnElementChanged(e);

            if (Element is WriteReviewView writeReviewView)
            {
                MoveView(writeReviewView);
            }
        }

        private void MoveView(WriteReviewView writeReviewView)
        {
            using (var notificationCenter = NSNotificationCenter.DefaultCenter)
            {
                notificationCenter.AddObserver(new NSString("UIKeyboardWillShowNotification"), (notification) =>
                {
                    var keyboardSize = UIKeyboard.FrameEndFromNotification(notification).Size;
                    var userInfo = notification.UserInfo;
                    var animationDuration = (userInfo[UIKeyboard.AnimationDurationUserInfoKey] as NSNumber).DoubleValue;
                    var safeAreaOffset = 0;

                    if (UIDevice.CurrentDevice.CheckSystemVersion(11, 0))
                    {
                        var window = UIApplication.SharedApplication.KeyWindow;
                        safeAreaOffset = (int)window.SafeAreaInsets.Bottom;
                    }

                    Animate(animationDuration, () =>
                    {
                        writeReviewView.Margin = new Thickness(0, 0, 0, keyboardSize.Height - safeAreaOffset);
                    });
                });

                notificationCenter.AddObserver(new NSString("UIKeyboardWillHideNotification"), (notification) =>
                {
                    var userInfo = notification.UserInfo;
                    var animationDuration = (userInfo[UIKeyboard.AnimationDurationUserInfoKey] as NSNumber).DoubleValue;

                    Animate(animationDuration, () =>
                    {
                        writeReviewView.Margin = new Thickness(0);
                    });
                });
            }
        }
    }
}
