﻿using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using eCommerce.Controls;
using eCommerce.iOS.Renderers;
using CoreGraphics;

[assembly: ExportRenderer(typeof(NoBorderEntry), typeof(NoBorderEntryRenderer))]
namespace eCommerce.iOS.Renderers
{
    public class NoBorderEntryRenderer : EntryRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.BackgroundColor = UIColor.Clear;
                Control.BorderStyle = UITextBorderStyle.None;
                Control.TintColor = UIColor.FromRGB(0, 158, 213);

                if (Element is NoBorderEntry noBorderEntry)
                {
                    if (!noBorderEntry.DisplaySuggestions)
                    {
                        Control.AutocorrectionType = UITextAutocorrectionType.No;
                    }

                    if (noBorderEntry.FloatingButton != null)
                    {
                        var accessoryView = ConvertToNative(noBorderEntry.FloatingButton, new CGRect(0, 0, noBorderEntry.FloatingButton.Width, noBorderEntry.FloatingButton.Height));

                        if (accessoryView.Hidden)
                        {
                            accessoryView.UserInteractionEnabled = true;
                        }

                        Control.InputAccessoryView = accessoryView;
                    }
                }
            }
        }

        public UIView ConvertToNative(View view, CGRect size)
        {
            var renderer = Platform.CreateRenderer(view);
            renderer.NativeView.Frame = size;

            var nativeView = renderer.NativeView;
            nativeView.SetNeedsLayout();

            return nativeView;
        }
    }
}