﻿using Foundation;
using eCommerce.Controls;
using eCommerce.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ImageButton), typeof(ImageButtonRenderer))]
namespace eCommerce.iOS.Renderers
{
    public class ImageButtonRenderer : ImageRenderer
    {
        private ImageButton _imageButton;

        protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
        {
            base.OnElementChanged(e);

            _imageButton = Element as ImageButton;

            if (_imageButton != null)
            {
                _imageButton.VerticalOptions = LayoutOptions.Center;
                _imageButton.HorizontalOptions = LayoutOptions.Center;                
            }
        }

        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            base.TouchesBegan(touches, evt);
            _imageButton?.OnButtonPressed();
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);

            var touch = touches.AnyObject as UITouch;
            var location = touch.LocationInView(this);

            if (Control.Frame.Contains(location))
            {
                _imageButton?.OnButtonReleased();
            }
        }

        public override void TouchesMoved(NSSet touches, UIEvent evt)
        {
            base.TouchesMoved(touches, evt);

            var touch = touches.AnyObject as UITouch;
            var location = touch.LocationInView(this);

            if (!Control.Frame.Contains(location))
            {
                _imageButton?.OnMovedOutside();
            }
        }
    }
}
