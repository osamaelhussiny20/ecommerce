﻿using CoreGraphics;
using Foundation;
using eCommerce.Controls;
using eCommerce.Enums;
using eCommerce.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(NoBorderButton), typeof(NoBorderButtonRenderer))]
namespace eCommerce.iOS.Renderers
{
    public class NoBorderButtonRenderer : ButtonRenderer
    {
        private const int Margin = 10;

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            if (Element is NoBorderButton noBorderButton)
            {
                if (Control != null)
                {
                    if (noBorderButton.TextAligment != ButtonTextAlignment.Default)
                    {
                        AlignText(noBorderButton);
                        AlignImage(noBorderButton);
                    }

                    if (noBorderButton.IsMultiline)
                    {
                        ManageMultilineText(noBorderButton);
                    }
                }
            }
        }

        private void AlignImage(NoBorderButton noBorderButton)
        {
            Control.ImageView.TranslatesAutoresizingMaskIntoConstraints = false;
            Control.ImageView.CenterYAnchor.ConstraintEqualTo(CenterYAnchor, constant: 0.0f).Active = true;

            switch (noBorderButton.ContentLayout.Position)
            {
                case Button.ButtonContentLayout.ImagePosition.Right:
                    Control.ImageView.RightAnchor.ConstraintEqualTo(RightAnchor, constant: -Margin).Active = true;
                    break;
                case Button.ButtonContentLayout.ImagePosition.Left:
                    Control.ImageView.LeftAnchor.ConstraintEqualTo(LeftAnchor, constant: Margin).Active = true;
                    break;
            }
        }

        //private void ResizeView(CGRect originFrame, UIWindow window)
        //{
        //    using (var notificationCenter = NSNotificationCenter.DefaultCenter)
        //    {
        //        notificationCenter.AddObserver(new NSString("UIKeyboardWillShowNotification"), (notification) =>
        //        {
        //            var keyboardSize = UIKeyboard.FrameEndFromNotification(notification).Size;
        //            var rootFrame = window.RootViewController.View.Frame;

        //            if (rootFrame.Height > originFrame.Height - keyboardSize.Height)
        //            {
        //                window.RootViewController.View.Frame = new CGRect(rootFrame.X, rootFrame.Y, rootFrame.Width, rootFrame.Height - keyboardSize.Height);
        //            }
        //        });

        //        notificationCenter.AddObserver(new NSString("UIKeyboardWillHideNotification"), (notification) =>
        //        {
        //            var keyboardSize = UIKeyboard.FrameEndFromNotification(notification).Size;
        //            var rootFrame = window.RootViewController.View.Frame;

        //            window.RootViewController.View.Frame = new CGRect(rootFrame.X, rootFrame.Y, rootFrame.Width, rootFrame.Height + keyboardSize.Height);
        //        });
        //    }
        //}

        private void ManageMultilineText(NoBorderButton noBorderButton)
        {
            var firstLineAttribute = new UIStringAttributes
            {
                Font = UIFont.FromName(noBorderButton.FontFamily, 10f)
            };

            var secondLineAttribute = new UIStringAttributes
            {
                Font = UIFont.FromName(noBorderButton.FontFamily, (float)noBorderButton.FontSize)
            };

            var attributedString = new NSMutableAttributedString();
            var multilineText = noBorderButton.Text.Split('\n');

            attributedString.Append(new NSAttributedString($"{multilineText[0]}\n", firstLineAttribute));
            attributedString.Append(new NSAttributedString(multilineText[1], secondLineAttribute));

            Control.TitleLabel.AttributedText = attributedString;
            Control.TitleLabel.LineBreakMode = UILineBreakMode.WordWrap;
            Control.TitleLabel.Lines = 0;
            Control.TitleLabel.TextAlignment = UITextAlignment.Center;
        }

        private void AlignText(NoBorderButton noBorderButton)
        {
            switch (noBorderButton.TextAligment)
            {
                case ButtonTextAlignment.Default:
                    break;
                case ButtonTextAlignment.Left:
                    Control.HorizontalAlignment = UIControlContentHorizontalAlignment.Left;
                    Control.ContentEdgeInsets = new UIEdgeInsets(0, Margin, 0, 0);
                    break;
                case ButtonTextAlignment.Center:
                    Control.HorizontalAlignment = UIControlContentHorizontalAlignment.Center;
                    break;
                case ButtonTextAlignment.Right:
                    Control.HorizontalAlignment = UIControlContentHorizontalAlignment.Right;
                    Control.ContentEdgeInsets = new UIEdgeInsets(0, 0, 0, Margin);
                    break;
            }
        }
    }
}

