﻿using eCommerce.Controls;
using eCommerce.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(RoundedView), typeof(RoundedViewRenderer))]
namespace eCommerce.iOS.Renderers
{
    public class RoundedViewRenderer : ViewRenderer<RoundedView, UIView>
    {
        protected override void OnElementChanged(ElementChangedEventArgs<RoundedView> e)
        {
            base.OnElementChanged(e);

            if (Element is RoundedView roundedView)
            {
                NativeView.Layer.CornerRadius = roundedView.BorderRadius;
                NativeView.Layer.BorderWidth = roundedView.BorderThickness;
                NativeView.Layer.BorderColor = roundedView.BorderColor.ToCGColor();
                NativeView.Layer.BackgroundColor = roundedView.BackgroundColor.ToCGColor();
            }
        }
    }
}
