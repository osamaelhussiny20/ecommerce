﻿using Foundation;
using eCommerce.Controls;
using eCommerce.iOS.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ClickableLabel), typeof(ClickableLabelRenderer))]
namespace eCommerce.iOS.Renderers
{
    public class ClickableLabelRenderer : LabelRenderer
    {
        private ClickableLabel _clickableLabel;

        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            _clickableLabel = Element as ClickableLabel;
        }

        public override void TouchesBegan(NSSet touches, UIEvent evt)
        {
            base.TouchesBegan(touches, evt);
            _clickableLabel?.OnPressed();
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);

            var touch = touches.AnyObject as UITouch;
            var location = touch.LocationInView(this);

            if (Control.Frame.Contains(location))
            {
                _clickableLabel?.OnReleased();
            }
        }

        public override void TouchesMoved(NSSet touches, UIEvent evt)
        {
            base.TouchesMoved(touches, evt);

            var touch = touches.AnyObject as UITouch;
            var location = touch.LocationInView(this);

            if (!Control.Frame.Contains(location))
            {
                _clickableLabel?.OnMovedOutside();
            }
        }
    }
}
