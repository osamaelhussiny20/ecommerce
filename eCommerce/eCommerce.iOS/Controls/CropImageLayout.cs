﻿using System;
using CoreGraphics;
using UIKit;
namespace eCommerce.iOS.Controls
{
    public class CropImageLayout : UIView
    {
        private CropImageNative imageControl;
        private CircleLayout circleLayout;
        private bool _didLoad;
        private string _imagePath;

        public CropImageLayout(CGRect frame, string imagePath)
        {
            _imagePath = imagePath;

        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            if (!_didLoad)
            {
                imageControl = new CropImageNative(Frame, _imagePath);
                circleLayout = new CircleLayout(Frame);
                circleLayout.UserInteractionEnabled = false;
                AddSubview(imageControl);
                AddSubview(circleLayout);
                BringSubviewToFront(circleLayout);
                _didLoad = true;
            }
        }

        public void LoadImage(string imagePath)
        {
            _imagePath = imagePath;
            if (imageControl != null)
            {
                imageControl = new CropImageNative(Frame, _imagePath);
                imageControl.LoadImage(imagePath);
            }
        }

        public byte[] GetCroppedImage()
        {
            return imageControl.GetCroppedImage();
        }
    }
}
