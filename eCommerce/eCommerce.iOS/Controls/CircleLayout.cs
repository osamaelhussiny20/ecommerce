﻿using System;
using CoreGraphics;
using UIKit;

namespace eCommerce.iOS.Controls
{
    public class CircleLayout : UIView
    {
        public CircleLayout(CGRect frame) : base(frame)
        {
        }

        public override void Draw(CGRect rect)
        {
            base.Draw(rect);

            var gctx = UIGraphics.GetCurrentContext();
            var color = UIColor.FromRGBA(25, 52, 65, 120);
            gctx.SetFillColor(color.CGColor);
            gctx.FillRect(rect);

            gctx.SetBlendMode(CGBlendMode.Clear);
            UIColor.Clear.SetColor();

            var path = new CGPath();
            nfloat offset = 30;
            nfloat size = Frame.Width - offset;
            nfloat xOffset = (Frame.Width /2) -( size / 2);
            nfloat yOffset = (Frame.Height / 2) - (size / 2);
            var circleRect = new CGRect(xOffset, yOffset, size, size);
            path.AddRoundedRect(circleRect, size / 2, size / 2);
            path.CloseSubpath();

            gctx.AddPath(path);
            gctx.DrawPath(CGPathDrawingMode.Fill);
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();
            BackgroundColor = UIColor.FromRGBA(0,0,0,0);
        }
    }
}
