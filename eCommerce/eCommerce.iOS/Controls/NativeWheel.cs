﻿using CoreGraphics;
using System;
using eCommerce.Helpers;
using UIKit;

namespace eCommerce.iOS.Control
{
    public class NativeWheel : UIImageView
    {
        private float _startAngle;
        private float _spinAmount;

        private Action<float> _updateSpinAmount;

        public NativeWheel(string iconSource, Action<float> updateAction)
        {
            _updateSpinAmount = updateAction;

            Image = UIImage.FromBundle(iconSource);
            UserInteractionEnabled = true;

            var panGestureRecognizer = new UIPanGestureRecognizer((recognizer) => PanDetected(recognizer))
            {
                MinimumNumberOfTouches = 1
            };

            AddGestureRecognizer(panGestureRecognizer);
        }

        private void PanDetected(UIPanGestureRecognizer recognizer)
        {
            if (recognizer.State == UIGestureRecognizerState.Began)
            {
                var location = recognizer.LocationInView(this);

                var dx = location.X - Center.X;
                var dy = location.Y - Center.Y;

                _startAngle = (float)Math.Atan2(dy, dx);
            }

            if (recognizer.State == UIGestureRecognizerState.Changed)
            {
                var location = recognizer.LocationInView(this);

                var dx = location.X - Center.X;
                var dy = location.Y - Center.Y;

                var currentAngle = (float)Math.Atan2(dy, dx);
                var angleDifference = _startAngle - currentAngle;

                Transform = CGAffineTransform.Rotate(Transform, -angleDifference);
                SetSpinAmount(-angleDifference);
            }
        }

        private void SetSpinAmount(float angleDifference)
        {
            var angleDifferenceInDegree = (float)MathHelper.RadianToDegree(angleDifference);
            var spinAmount = _spinAmount + angleDifferenceInDegree;

            if (spinAmount > 0)
            {
                _spinAmount += angleDifferenceInDegree;
            }

            _updateSpinAmount?.Invoke(_spinAmount);
        }
    }
}
