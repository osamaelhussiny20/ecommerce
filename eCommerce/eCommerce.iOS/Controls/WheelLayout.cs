﻿using CoreGraphics;
using System;
using UIKit;

namespace eCommerce.iOS.Control
{
    public class WheelLayout : UIView
    {
        private const double ProfilePictureOffsetModifier = 0.4;

        private bool _didLoad;
        private UIImage _profilePicture;
        private string _backgroundImageSource;
        private NativeWheel _nativeWheel;
        private UIImageView _profilePictureView;
        private Action<float> _updateAction;

        public WheelLayout(UIImage profilePicture, string backgroundImageSource, Action<float> updateAction)
        {
            _profilePicture = profilePicture;
            _backgroundImageSource = backgroundImageSource;
            _updateAction = updateAction;
        }

        public override void LayoutSubviews()
        {
            base.LayoutSubviews();

            if (!_didLoad)
            {
                var centerX = Frame.GetMidX() - Frame.Width / 2;
                var centerY = Frame.GetMidY() - Frame.Width / 2;

                var profilePictureOffset = Frame.Width * ProfilePictureOffsetModifier;

                var nativeWheelFrame = new CGRect(centerX, centerY, Frame.Width, Frame.Width);

                _nativeWheel = new NativeWheel(_backgroundImageSource, _updateAction)
                {
                    Frame = nativeWheelFrame
                };

                var profilePictureViewSize = _nativeWheel.Frame.Width - profilePictureOffset;

                _profilePictureView = new UIImageView(_profilePicture)
                {
                    Frame = new CGRect(_nativeWheel.Frame.GetMidX() - profilePictureViewSize / 2, _nativeWheel.Frame.GetMidY() - profilePictureViewSize / 2,
                    profilePictureViewSize, profilePictureViewSize)
                };

                _profilePictureView.Layer.CornerRadius = _profilePictureView.Frame.Width / 2;
                _profilePictureView.Layer.MasksToBounds = true;

                AddSubview(_nativeWheel);
                AddSubview(_profilePictureView);

                _didLoad = true;
            }
        }
    }
}
