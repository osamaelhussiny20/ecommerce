﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace eCommerce.iOS.Controls
{
    public class CropImageNative : UIImageView
    {
        nfloat r = 0;
        nfloat dx = 0;
        nfloat dy = 0;
        nfloat x = 0;
        nfloat y = 0;
        nfloat s0 = 1;
        nfloat realSceenWidth;
        nfloat realSceenHeight;
        nfloat scaleFactor;
        nfloat baseXOffset;
        nfloat baseYOffset;
        nfloat rectLenght;

        public CGRect cropRect;

        UIPinchGestureRecognizer _pinchGestureRecognizer;
        UILongPressGestureRecognizer _longPressGestureRecognizer;

        public CropImageNative(CGRect frame, string imagePath) : base(frame)
        {
            ContentMode = UIViewContentMode.ScaleAspectFit;
            TranslatesAutoresizingMaskIntoConstraints = false;
            UserInteractionEnabled = true;

            var panGestureRecognizer = new UIPanGestureRecognizer((recognizer) => PanDetected(recognizer))
            {
                MinimumNumberOfTouches = 1
            };
            _pinchGestureRecognizer = new UIPinchGestureRecognizer(PinchDetected);

            AddGestureRecognizer(panGestureRecognizer);
            AddGestureRecognizer(_pinchGestureRecognizer);

            if (!string.IsNullOrEmpty(imagePath))
            {
                LoadImage(imagePath);
            }
        }

        public void LoadImage(string imagePath)
        {
            Image = UIImage.FromFile(imagePath);

            realSceenWidth = Frame.Width * UIScreen.MainScreen.Scale;
            realSceenHeight = Frame.Height * UIScreen.MainScreen.Scale;

            scaleFactor = (nfloat)(Math.Min(realSceenHeight, realSceenWidth) / Math.Min(Image.Size.Width, Image.Size.Height));

            baseXOffset = realSceenWidth - (Image.Size.Width * scaleFactor);
            baseYOffset = realSceenHeight - (Image.Size.Height * scaleFactor);
            baseXOffset /= 2;
            baseYOffset /= 2;
            var rectOffset = 30;
            rectLenght = (Frame.Width - rectOffset) * UIScreen.MainScreen.Scale;            
        }

        private void PinchDetected()
        {
            if (_pinchGestureRecognizer.State == UIGestureRecognizerState.Began || 
                _pinchGestureRecognizer.State == UIGestureRecognizerState.Changed)
            {
                _pinchGestureRecognizer.View.Transform *= CGAffineTransform.MakeScale(_pinchGestureRecognizer.Scale, _pinchGestureRecognizer.Scale);
                scaleFactor *= _pinchGestureRecognizer.Scale;
                _pinchGestureRecognizer.Scale = 1;
            }
        }

        private void PanDetected(UIPanGestureRecognizer recognizer)
        {
            if ((recognizer.State == UIGestureRecognizerState.Began || recognizer.State == UIGestureRecognizerState.Changed) && (recognizer.NumberOfTouches == 1))
            {
                var p0 = recognizer.LocationInView(null);

                if (dx == 0)
                    dx = p0.X - Center.X;

                if (dy == 0)
                    dy = p0.Y - Center.Y;

                var p1 = new CGPoint(p0.X - dx, p0.Y - dy);
                x = p0.X;
                y = p0.Y;

                Center = p1;
            }
            else if (recognizer.State == UIGestureRecognizerState.Ended)
            {
                dx = 0;
                dy = 0;
            }
        }

        public byte [] GetCroppedImage()
        {
            var translateX = Frame.X * 2 / scaleFactor;
            var translateY = Frame.Y * 2 / scaleFactor;

            var actualImageWidth = Image.Size.Width / (Frame.Width * UIScreen.MainScreen.Scale);
            var actualImageHeight = Image.Size.Height / (Frame.Height * UIScreen.MainScreen.Scale);

            var rectXPos = (realSceenWidth - rectLenght) / 2 ;
            var rectYPos = (realSceenHeight - rectLenght) / 2;
            rectXPos -= baseXOffset ;
            rectYPos -= baseYOffset ;
            rectXPos /= scaleFactor;
            rectYPos /= scaleFactor;

            var transf = Transform;

            var xPos = translateX > 0.0 ? rectXPos - Math.Abs(translateX) : rectXPos + Math.Abs(translateX);
            var yPos = translateY > 0.0 ? rectYPos - Math.Abs(translateY) : rectYPos + Math.Abs(translateY);

            cropRect = new CGRect(xPos, yPos, rectLenght / scaleFactor, rectLenght / scaleFactor);

            var inputCGImage = Image.CGImage;

            var image = inputCGImage.WithImageInRect(cropRect);

            //TODO: remove after finish
            var centerX = Frame.Width / 2;
            var centerY = Frame.Height / 2;
            var center = new CGPoint(centerX, centerY);

            Byte[] croppedImageBytes;
            using (var croppedImage = UIImage.FromImage(image))
            {
                //Remove after finish
                Image = croppedImage;
                Center = center;

                using (NSData imageData = croppedImage.AsJPEG())
                {
                    croppedImageBytes = new Byte[imageData.Length];
                    System.Runtime.InteropServices.Marshal.Copy(imageData.Bytes, croppedImageBytes, 0, Convert.ToInt32(imageData.Length));
                }
            }
            
            return croppedImageBytes;
        }
    }
}