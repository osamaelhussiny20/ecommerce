﻿using CoreGraphics;
using System;
using UIKit;

namespace eCommerce.iOS.Control
{
    public class NativeSwipeButton : UIView
    {
        private const double WidthInterceptionInPercent = 0.16;
        private const double VelocityMultiplyValue = 0.2;
        private const double AnimationDuration = 0.2;
        private const double AnimationMultiplyValue = 0.0002;
        private const float FadeDecrement = 1.5f;

        private UIView _dragButton;
        private UILabel _buttonLabel;
        private UILabel _subButtonLabel;
        private UIImageView _imageView;
        private bool _swipeCompleted;
        private bool _layoutSet;
        private string _buttonText;
        private string _description;
        private int _dragButtonSize;
        private UIColor _backgroundColor;
        private UIColor _dragButtonBackgroundColor;
        private UIColor _textColor;
        private string _iconSource;
        private string _fontName;
        private float _fontSize;
        private float _descriptionFontSize;

        public Action SwipeComplited;

        public NativeSwipeButton(int dragButtonSize, string buttonText, string description, string iconSource, UIColor buttonBackgroundColor, UIColor dragButtonBackgroundColor, UIColor textColor,
            string fontName, float fontSize, float descriptionFontSize)
        {
            _dragButton = new UIView();
            _buttonLabel = new UILabel();
            _imageView = new UIImageView();

            _buttonText = buttonText;
            _description = description;
            _dragButtonSize = dragButtonSize;

            _backgroundColor = buttonBackgroundColor;
            _dragButtonBackgroundColor = dragButtonBackgroundColor;
            _iconSource = iconSource;
            _textColor = textColor;

            _fontName = fontName;
            _fontSize = fontSize;
            _descriptionFontSize = descriptionFontSize;
        }

        public override void LayoutSubviews()
        {
            if (!_layoutSet)
            {
                SetUpButton();
                _layoutSet = true;
            }
        }

        public void ResetButton()
        {
            ResetSwipe();
        }

        private void SetUpButton()
        {
            BackgroundColor = _backgroundColor;
            Layer.CornerRadius = Frame.Height / 2;

            _dragButton = new UIView(new CGRect(0, 0, _dragButtonSize, Frame.Size.Height))
            {
                BackgroundColor = _dragButtonBackgroundColor
            };

            _dragButton.Layer.CornerRadius = _dragButton.Frame.Height / 2;

            AddSubview(_dragButton);

            if (!string.IsNullOrEmpty(_buttonText))
            {
                _buttonLabel = new UILabel(new CGRect(0, 0, Frame.Size.Width, Frame.Size.Height))
                {
                    TextAlignment = UITextAlignment.Center,
                    Text = _buttonText,
                    TextColor = _textColor,
                    Font = UIFont.FromName(_fontName, _fontSize) ?? UIFont.SystemFontOfSize(_fontSize),
                    TranslatesAutoresizingMaskIntoConstraints = false
                };

                _subButtonLabel = new UILabel(new CGRect(0, 0, Frame.Size.Width, Frame.Size.Height))
                {
                    TextAlignment = UITextAlignment.Center,
                    Text = _description,
                    TextColor = _textColor,
                    Font = UIFont.FromName(_fontName, _descriptionFontSize) ?? UIFont.SystemFontOfSize(_descriptionFontSize),
                    TranslatesAutoresizingMaskIntoConstraints = false
                };

                AddSubview(_buttonLabel);
                AddSubview(_subButtonLabel);

                UpdateConstraits();
            }

            BringSubviewToFront(_dragButton);

            if (!string.IsNullOrEmpty(_iconSource))
            {
                _imageView = new UIImageView(new CGRect(0, 0, _dragButtonSize, Frame.Size.Height))
                {
                    ContentMode = UIViewContentMode.Center,
                    Image = UIImage.FromBundle(_iconSource)
                };
                _dragButton.AddSubview(_imageView);
            }

            Layer.MasksToBounds = true;

            var panGestureRecognizer = new UIPanGestureRecognizer((recognizer) => PanDetected(recognizer))
            {
                MinimumNumberOfTouches = 1
            };

            _dragButton.AddGestureRecognizer(panGestureRecognizer);
        }

        private void UpdateConstraits()
        {
            NSLayoutConstraint.Create(_buttonLabel, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterX, 1.0f, 0.0f).Active = true;
            NSLayoutConstraint.Create(_subButtonLabel, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterX, 1.0f, 0.0f).Active = true;

            NSLayoutConstraint.Create(_buttonLabel, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, this, NSLayoutAttribute.CenterY, 1.0f, -_fontSize / 2).Active = true;
            NSLayoutConstraint.Create(_subButtonLabel, NSLayoutAttribute.Top, NSLayoutRelation.Equal, _buttonLabel, NSLayoutAttribute.Bottom, 1.0f, 0.0f).Active = true;
        }

        private void PanDetected(UIPanGestureRecognizer recognizer)
        {
            var translatedPoint = recognizer.TranslationInView(this);
            translatedPoint = new CGPoint(translatedPoint.X, Frame.Size.Height / 2);

            recognizer.View.Frame = new CGRect(translatedPoint.X, recognizer.View.Frame.Y, _dragButtonSize, recognizer.View.Frame.Height);

            _buttonLabel.Alpha = (1 - FadeDecrement * (recognizer.View.Frame.X + recognizer.View.Frame.Width) / Frame.Width);
            _subButtonLabel.Alpha = (1 - FadeDecrement * (recognizer.View.Frame.X + recognizer.View.Frame.Width) / Frame.Width);

            if (recognizer.State == UIGestureRecognizerState.Ended)
            {
                var velocityX = recognizer.VelocityInView(this).X;

                if (translatedPoint.X < 0)
                {
                    translatedPoint.X = 0;
                }
                else if (translatedPoint.X + _dragButtonSize > (Frame.Size.Width - Frame.Size.Width * WidthInterceptionInPercent))
                {
                    _swipeCompleted = true;
                    CompliteSwipe();
                }

                var animationDuration = Math.Abs((velocityX) * AnimationMultiplyValue) + AnimationDuration;

                Transition(this, animationDuration, UIViewAnimationOptions.CurveEaseOut, null, () =>
                {
                    AnimationFinished();
                });
            }
        }

        private void AnimationFinished()
        {
            if (!_swipeCompleted)
            {
                ResetSwipe();
            }
        }

        private void CompliteSwipe()
        {
            Transition(this, AnimationDuration, UIViewAnimationOptions.CurveEaseOut, () =>
            {
                _dragButton.Frame = new CGRect(Frame.Size.Width - _dragButton.Frame.Width, 0, _dragButtonSize, _dragButton.Frame.Size.Height);
            }, () =>
            {
                _dragButton.BackgroundColor = _dragButtonBackgroundColor;
                _swipeCompleted = false;
                SwipeComplited?.Invoke();
            });
        }

        private void ResetSwipe()
        {
            Transition(this, AnimationDuration, UIViewAnimationOptions.CurveEaseOut, () =>
            {
                _dragButton.Frame = new CGRect(0, 0, _dragButtonSize, _dragButton.Frame.Size.Height);
            }, () =>
            {
                _dragButton.BackgroundColor = _dragButtonBackgroundColor;
                _buttonLabel.Alpha = 1;
                _subButtonLabel.Alpha = 1;
                _swipeCompleted = false;
            });
        }
    }
}