﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using eCommerce.Droid.Services;
using eCommerce.Services;
using Xamarin.Forms;

[assembly:Dependency(typeof(DeviceOrientationImplementation))]
namespace eCommerce.Droid.Services
{
    public class DeviceOrientationImplementation : IDeviceOrientation
    {
        public DeviceOrientationImplementation(){}
        public static void Init() { }
        public DeviceOrientations GetOrientation()
        {
            IWindowManager windowManager = Android.App.Application.Context.GetSystemService(Context.WindowService).JavaCast<IWindowManager>();
            var rotation = windowManager.DefaultDisplay.Rotation;
            bool isLandscape = rotation == SurfaceOrientation.Rotation90 || rotation == SurfaceOrientation.Rotation270;
            return isLandscape ? DeviceOrientations.Landscape : DeviceOrientations.Portrait;
        }

        public void SetLandscape()
        {
           MainActivity.Instance.RequestedOrientation = ScreenOrientation.Landscape;
        }

        public void SetPortrait()
        {
            MainActivity.Instance.RequestedOrientation = ScreenOrientation.Portrait;
        }
    }
}