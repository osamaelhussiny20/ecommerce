﻿using System;
using eCommerce.Enums;
using eCommerce.Services.UserDialog;
using eCommerce.Droid.Services;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using static Android.App.ActionBar;
using RelativeLayout = Android.Widget.RelativeLayout;
using View = Android.Views.View;
using Android.Views;
using Android.Animation;
using Android.Widget;

[assembly: Dependency(typeof(ToastService))]

namespace eCommerce.Droid.Services
{
    public class ToastService : IToastService, IDisposable
    {
        private const int DurationMultiplier = 1000;
        private int _toastPading = 20;
        private Color _backgroundColor;
        private bool _isShowing;
        private ViewGroup _mainView;
        private View _contentView;
        private RelativeLayout _toastView;
        private ValueAnimator _slideDownAnimator;
        private ValueAnimator _slideUpAnimator;
        private int _duration;
        private ToastType _toastType;

        public ToastService()
        {
            _mainView = MainActivity.Instance.Window.DecorView.FindViewById(Android.Resource.Id.Content) as ViewGroup;
        }

        public void ShowToast(string message, ToastType toastType, int duration = 5, int fontSize = 14, Color? backgroundColor = null, Color? textColor = null)
        {
            _duration = duration;
            _toastType = toastType;

            if (_toastType == ToastType.Alert)
            {
                _backgroundColor = Color.FromHex("#C72B41");
            }
            else if (_toastType == ToastType.Notification)
            {
                _backgroundColor = Color.FromHex("#00A651");
                _toastPading = 5;
            }
            if (_toastType == ToastType.Offline)
            {
                _backgroundColor = Color.FromHex("#F31431");
            }

            if (!_isShowing)
            {
                _isShowing = true;

                Buildlayout(message, fontSize, backgroundColor, textColor);
                _mainView.ViewTreeObserver.GlobalLayout += ViewTreeObserver_GlobalLayout;
            }
        }

        private void ViewTreeObserver_GlobalLayout(object sender, EventArgs e)
        {
            _mainView.ViewTreeObserver.GlobalLayout -= ViewTreeObserver_GlobalLayout;

            if (_toastView != null)
            {
                var toastHeight = _toastView.Height;

                var animationDuration = _duration * DurationMultiplier;
                AnimateToast(animationDuration, toastHeight);
            }
        }

        private void Buildlayout(string message, int fontSize, Color? backgroundColor, Color? textColor)
        {
            _toastView = new RelativeLayout(_mainView.Context);

            LayoutParams toastLayoutParams = new LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.WrapContent);
            _toastView.LayoutParameters = toastLayoutParams;
            _toastView.SetPadding(0, (int)Utils.ConvertDpToPixel(_mainView.Context, _toastPading), 0, (int)Utils.ConvertDpToPixel(_mainView.Context, _toastPading));

            var convertedBackgroundColor = (backgroundColor ?? _backgroundColor).ToAndroid();
            _toastView.SetBackgroundColor(convertedBackgroundColor);

            var toastTextView = new TextView(_mainView.Context)
            {
                Text = message,
                TextSize = fontSize,
                Gravity = GravityFlags.Center
            };

            var convertedTextColor = (textColor ?? Color.White).ToAndroid();
            toastTextView.SetTextColor(convertedTextColor);

            RelativeLayout.LayoutParams toastTextViewLayoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
            toastTextViewLayoutParams.AddRule(LayoutRules.CenterInParent);
            _toastView.AddView(toastTextView, toastTextViewLayoutParams);

            if (_toastType == ToastType.Alert)
            {
                _mainView.AddView(_toastView);
            }
            else
            {
                BuildNotification();
            }
        }

        private void BuildNotification()
        {
            MainActivity.Instance.Window.SetStatusBarColor(_backgroundColor.ToAndroid());

            var parentLayout = new LinearLayout(_mainView.Context)
            {
                Orientation = Orientation.Vertical
            };

            _contentView = _mainView.GetChildAt(0);
            _contentView.RemoveFromParent();

            parentLayout.AddView(_toastView);
            parentLayout.AddView(_contentView);

            _mainView.AddView(parentLayout);
        }

        private void AnimateToast(int animationDuration, int toastHeight)
        {
            _slideDownAnimator = ValueAnimator.OfFloat(-toastHeight, 0);
            _slideDownAnimator.SetDuration(animationDuration / 10);

            _slideUpAnimator = ValueAnimator.OfFloat(0, -toastHeight);
            _slideUpAnimator.SetDuration(animationDuration / 10);
            _slideUpAnimator.StartDelay = ((long)(animationDuration / 2.5f));

            _slideDownAnimator.Update += SlideDownAnimator_Update;
            _slideUpAnimator.Update += SlideUpAnimator_Update;
            _slideDownAnimator.AnimationEnd += SlideDownAnimator_AnimationEnd;
            _slideUpAnimator.AnimationEnd += SlideUpAnimator_AnimationEnd;

            _slideDownAnimator.Start();
        }

        private void SlideUpAnimator_AnimationEnd(object sender, EventArgs e)
        {
            _isShowing = false;

            if (_toastType == ToastType.Alert)
            {
                _mainView.RemoveView(_toastView);
            }
            else
            {
                _contentView.TranslationY += _toastView.Height;
                _toastView.RemoveFromParent();
            }
        }

        private void SlideUpAnimator_Update(object sender, ValueAnimator.AnimatorUpdateEventArgs e)
        {
            float y = (float)e.Animation.AnimatedValue;
            _toastView.SetY(y);

            if (_toastType != ToastType.Alert)
            {
                _contentView.TranslationY = y;
            }
        }

        private void SlideDownAnimator_AnimationEnd(object sender, EventArgs e)
        {
            if (_toastType == ToastType.Alert)
            {
                _slideUpAnimator.Start();
            }
        }

        private void SlideDownAnimator_Update(object sender, ValueAnimator.AnimatorUpdateEventArgs e)
        {
            float y = (float)e.Animation.AnimatedValue;
            _toastView.SetY(y);

            if (_toastType != ToastType.Alert)
            {
                _contentView.TranslationY = y;
            }
        }

        public void Dispose()
        {
            _mainView.ViewTreeObserver.GlobalLayout -= ViewTreeObserver_GlobalLayout;
            _slideDownAnimator.AnimationEnd -= SlideDownAnimator_AnimationEnd;
            _slideDownAnimator.Update -= SlideDownAnimator_Update;
            _slideUpAnimator.AnimationEnd -= SlideUpAnimator_AnimationEnd;
            _slideUpAnimator.Update -= SlideUpAnimator_Update;
        }

        public void DismissNotification(int duration)
        {
            if (_slideUpAnimator != null)
            {
                _slideUpAnimator.StartDelay = 0;
                _slideUpAnimator.Start();

                var statusBarColor = _mainView.Resources.GetColor(Resource.Color.error_color_material, _mainView.Context.Theme);
                MainActivity.Instance.Window.SetStatusBarColor(statusBarColor);
            }
        }
    }

}