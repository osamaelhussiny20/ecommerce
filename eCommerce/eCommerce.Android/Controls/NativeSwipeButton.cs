﻿using System;
using Android.Animation;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Runtime;
using Android.Support.V4.Content;
using Android.Views;
using Android.Views.Animations;
using Android.Widget;

namespace eCommerce.Droid.Controls
{
    public class NativeSwipeButton : RelativeLayout
    {
        private const double WidthInterceptionInPercent = 0.16;
        private const float FadeDecrement = 1.5f;
        private const long AnimationDuration = 200;

        private ImageView _dragButtonImage;
        private float _initialXPosition;
        private bool _swipeCompleted;
        private TextView _buttonTextView;
        private TextView _descriptionTextView;
        private ViewGroup _viewGroup;
        private string _buttonText;
        private string _descriptionText;
        private int _dragButtonSize;
        private int _viewHeight;
        private Color _backgroundColor;
        private Color _dragButtonBackgroundColor;
        private Color _textColor;
        private string _iconSource;
        private string _fontName;
        private float _fontSize;
        private float _descriptionFontSize;

        public Action SwipeComplited;

        protected NativeSwipeButton(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public NativeSwipeButton(Context context, int viewHeight, int dragButtonSize, string buttonText, string descriptionText, string iconSource, Color buttonBackgroundColor, Color dragButtonBackgroundColor, Color textColor,
            string fontName, float fontSize, float descriptionFontSize) : base(context)
        {
            _buttonText = buttonText;
            _descriptionText = descriptionText;
            _dragButtonSize = dragButtonSize;
            _viewHeight = viewHeight;

            _backgroundColor = buttonBackgroundColor;
            _dragButtonBackgroundColor = dragButtonBackgroundColor;
            _iconSource = iconSource;
            _textColor = textColor;

            _fontName = fontName;
            _fontSize = fontSize;
            _descriptionFontSize = descriptionFontSize;

            InitViewGroup(context);
            InitButtonTextView(context);
            InitDragButton(context);

            Touch += NativeSwipeButton_Touch;
        }

        public void ResetButton()
        {
            CollapseSwipe();
            ResetSwipe();
        }

        private void InitViewGroup(Context context)
        {
            _viewGroup = new RelativeLayout(context);

            var backgroundDrawable = new GradientDrawable();
            backgroundDrawable.SetCornerRadius(Utils.ConvertDpToPixel(context, _viewHeight) / 2);
            backgroundDrawable.SetColor(_backgroundColor);

            _viewGroup.Background = backgroundDrawable;

            LayoutParams layoutParamsView = new LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent);
            layoutParamsView.AddRule(LayoutRules.CenterInParent);

            AddView(_viewGroup, layoutParamsView);
        }

        private void InitButtonTextView(Context context)
        {
            _buttonTextView = new TextView(context)
            {
                Text = _buttonText,
                TextSize = _fontSize,
                Gravity = GravityFlags.Center
            };

            _descriptionTextView = new TextView(context)
            {
                Text = _descriptionText,
                TextSize = _descriptionFontSize,
                Gravity = GravityFlags.Center
            };

            _buttonTextView.SetTextColor(_textColor);
            _descriptionTextView.SetTextColor(_textColor);

            if (string.IsNullOrEmpty(_fontName))
            {
                _buttonTextView.Typeface = Typeface.Default;
                _descriptionTextView.Typeface = Typeface.Default;
            }
            else
            {
                var typeface = Typeface.CreateFromAsset(context.Assets, _fontName);
                _buttonTextView.Typeface = typeface;
                _descriptionTextView.Typeface = typeface;
            }

            var textLayout = new LinearLayout(context)
            {
                Orientation = Orientation.Vertical
            };

            textLayout.AddView(_buttonTextView);
            textLayout.AddView(_descriptionTextView);

            LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
            layoutParams.AddRule(LayoutRules.CenterInParent);

            _viewGroup.AddView(textLayout, layoutParams);
        }

        private void InitDragButton(Context context)
        {
            _dragButtonImage = new ImageView(context);

            var dragButtonSize = (int)Utils.ConvertDpToPixel(context, _dragButtonSize);

            var dragButtonDrawable = new GradientDrawable();
            dragButtonDrawable.SetCornerRadius(dragButtonSize / 2);
            dragButtonDrawable.SetColor(_dragButtonBackgroundColor);

            _dragButtonImage.Background = dragButtonDrawable;
            _dragButtonImage.SetScaleType(ImageView.ScaleType.Center);

            LayoutParams layoutParamsButton = new LayoutParams(dragButtonSize, dragButtonSize);
            layoutParamsButton.AddRule(LayoutRules.AlignParentLeft);
            layoutParamsButton.AddRule(LayoutRules.CenterVertical);

            if (!string.IsNullOrEmpty(_iconSource))
            {
                _dragButtonImage.SetImageDrawable(ContextCompat.GetDrawable(context, Resources.GetIdentifier(_iconSource, "drawable", context.PackageName)));
            }

            AddView(_dragButtonImage, layoutParamsButton);
        }

        private void NativeSwipeButton_Touch(object sender, TouchEventArgs e)
        {
            switch (e.Event.Action)
            {
                case MotionEventActions.Move:
                    MoveButton(e);
                    break;
                case MotionEventActions.Up:
                    EndButtonMovement();
                    break;
            }
        }

        private void MoveButton(TouchEventArgs e)
        {
            if (_initialXPosition == 0)
            {
                _initialXPosition = _dragButtonImage.GetX();
            }

            if (e.Event.GetX() > _dragButtonImage.Width / 2 && e.Event.GetX() + _dragButtonImage.Width / 2 < Width)
            {
                _dragButtonImage.SetX(e.Event.GetX() - _dragButtonImage.Width / 2);
                _buttonTextView.Alpha = (1 - 1.5f * (_dragButtonImage.GetX() + _dragButtonImage.Width) / Width);
                _descriptionTextView.Alpha = (1 - 1.5f * (_dragButtonImage.GetX() + _dragButtonImage.Width) / Width);
            }

            if (e.Event.GetX() + _dragButtonImage.Width / 2 > Width && _dragButtonImage.GetX() + _dragButtonImage.Width / 2 < Width)
            {
                _dragButtonImage.SetX(Width - _dragButtonImage.Width);
            }

            if (e.Event.GetX() < _dragButtonImage.Width / 2)
            {
                _dragButtonImage.SetX(0);
            }
        }

        private void EndButtonMovement()
        {
            if (_swipeCompleted)
            {
                CollapseSwipe();
            }
            else
            {
                if (_dragButtonImage.GetX() + _dragButtonImage.Width > Width - Width * WidthInterceptionInPercent)
                {
                    CompliteSwipe();
                }
                else
                {
                    ResetSwipe();
                }
            }
        }

        private void CompliteSwipe()
        {
            var positionAnimator = ValueAnimator.OfFloat(_dragButtonImage.GetX(), Width - _dragButtonImage.Width);
            positionAnimator.Update += CompliteSwipePositionAnimatorUpdate;
            positionAnimator.AnimationEnd += CompliteSwipePositionAnimatorEnd;
            positionAnimator.Start();
        }

        private void CompliteSwipePositionAnimatorUpdate(object sender, ValueAnimator.AnimatorUpdateEventArgs e)
        {
            float x = (float)e.Animation.AnimatedValue;
            _dragButtonImage.SetX(x);
        }

        private void CompliteSwipePositionAnimatorEnd(object sender, EventArgs e)
        {
            _swipeCompleted = true;
            SwipeComplited?.Invoke();
        }

        private void ResetSwipe()
        {
            var positionAnimator = ValueAnimator.OfFloat(_dragButtonImage.GetX(), 0);
            positionAnimator.SetInterpolator(new AccelerateDecelerateInterpolator());
            positionAnimator.Update += ResetSwipePositionAnimatorUpdate;

            var textAnimator = ObjectAnimator.OfFloat(_buttonTextView, "Alpha", 1);
            var descriptionAnimator = ObjectAnimator.OfFloat(_descriptionTextView, "Alpha", 1);

            positionAnimator.SetDuration(AnimationDuration);

            var animatorSet = new AnimatorSet();
            animatorSet.PlayTogether(textAnimator, descriptionAnimator, positionAnimator);
            animatorSet.Start();
        }

        private void ResetSwipePositionAnimatorUpdate(object sender, ValueAnimator.AnimatorUpdateEventArgs e)
        {
            float x = (float)e.Animation.AnimatedValue;
            _dragButtonImage.SetX(x);
        }

        private void CollapseSwipe()
        {
            var textAnimator = ObjectAnimator.OfFloat(_buttonTextView, "Alpha", 1);
            var descriptionAnimator = ObjectAnimator.OfFloat(_descriptionTextView, "Alpha", 1);

            textAnimator.AnimationEnd += CollapseSwipeAnimationEnd;

            var animatorSet = new AnimatorSet();
            animatorSet.PlayTogether(textAnimator, descriptionAnimator);
            animatorSet.Start();
        }

        private void CollapseSwipeAnimationEnd(object sender, EventArgs e)
        {
            _swipeCompleted = false;
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            Touch -= NativeSwipeButton_Touch;
        }
    }
}