﻿using System;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Views;

namespace eCommerce.Droid.Controls
{
    public class CircleLayout : View
    {
        public CircleLayout(IntPtr javaReference, JniHandleOwnership transfer) :
            base(javaReference, transfer) {}
        
        public CircleLayout (Context context ) : base(context)
        {
            SetLayerType(LayerType.Software, null);
            SetWillNotDraw(false);
        }

        protected override void OnDraw(Canvas canvas)
        {
            Paint paint = new Paint();
            paint.AntiAlias = true;
            paint.Color = Color.Argb(120, 25, 52, 65);
            canvas.DrawPaint(paint);
            paint.SetXfermode(new PorterDuffXfermode(PorterDuff.Mode.Clear));
            var offset = Utils.ConvertDpToPixel(Context, 40); ;
            var radius = (Width - offset) / 2;
            var cx = offset / 2 + radius;
            var cy = (Height / 2);
            canvas.DrawCircle(cx, cy, radius, paint);
        }
    }
}