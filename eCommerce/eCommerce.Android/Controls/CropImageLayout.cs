﻿using Android.Content;
using Android.Util;
using Android.Views;
using Android.Widget;
using System;

namespace eCommerce.Droid.Controls
{
    public class CropImageLayout : RelativeLayout
    {
        private ViewGroup _viewGroup;
        private CropImageNative _cropImageNative;
        public Action CropComplited;
        public CropImageLayout(Context context, IAttributeSet attrs) :
            base(context, attrs)
        {
        }

        public CropImageLayout(Context context, IAttributeSet attrs, int defStyle) :
            base(context, attrs, defStyle)
        {
        }

        public CropImageLayout (Context context, string imagePath) : base(context)
        {
            InitViewGroup(context, imagePath);
        }

        public byte[] GetCroppedImage()
        {
            return _cropImageNative.GetCroppedPicture();
        }

        public void InitializeImageControl(string imageSource)
        {
            _cropImageNative.InitializeNewImage(imageSource);
        }

        private void InitViewGroup(Context context, string imagePath)
        {
            _cropImageNative = new CropImageNative(context, imagePath);
            var circleLayout = new CircleLayout(context);
            
            LayoutParams layoutParamsView = new LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent);
            layoutParamsView.AddRule(LayoutRules.CenterInParent);

            AddView(_cropImageNative, layoutParamsView);
            AddView(circleLayout, layoutParamsView);
        }
    }
}