﻿using Android.Content;
using Android.Graphics;
using Android.Support.V4.Graphics.Drawable;
using Android.Views;
using Android.Widget;
using System;

namespace eCommerce.Droid.Controls
{
    public class WheelLayout : RelativeLayout
    {
        private const double ProfilePictureOffsetModifier = 0.38;
        private NativeWheel _nativeWheel;

        public WheelLayout(Context context, float width, float height, Bitmap profilePicture, string backgroundImage) : base(context)
        {
            var convertedHeight = (int)Utils.ConvertDpToPixel(context, height);
            var convertedWidht = (int)Utils.ConvertDpToPixel(context, width);

            InitWheel(context, convertedWidht, convertedHeight, backgroundImage);
            InitProfilePicture(context, convertedWidht, convertedHeight, profilePicture);
        }

        public void GetSpinAmount(Action<float> updateAction)
        {
            _nativeWheel.UpdateSpinAmount = updateAction;
        }

        private void InitWheel(Context context, int width, int height, string backgroundImage)
        {
            _nativeWheel = new NativeWheel(context, width, height, backgroundImage);

            LayoutParams layoutParamsView = new LayoutParams(ViewGroup.LayoutParams.MatchParent, ViewGroup.LayoutParams.MatchParent);
            layoutParamsView.AddRule(LayoutRules.CenterInParent);

            AddView(_nativeWheel, layoutParamsView);
        }

        private void InitProfilePicture(Context context, int width, int height, Bitmap profilePicture)
        {
            var profilePictureImageView = new ImageView(context);

            var circleDrawavle = RoundedBitmapDrawableFactory.Create(Resources, profilePicture);
            circleDrawavle.Circular = true;
            profilePictureImageView.SetImageDrawable(circleDrawavle);

            var profilePictureOffset = width * ProfilePictureOffsetModifier;
            var profilePictureWidth = width - profilePictureOffset;
            var profilePictureHeight = height - profilePictureOffset;

            LayoutParams layoutParamsView = new LayoutParams((int)profilePictureWidth, (int)profilePictureHeight);
            layoutParamsView.AddRule(LayoutRules.CenterInParent);

            AddView(profilePictureImageView, layoutParamsView);
        }
    }
}