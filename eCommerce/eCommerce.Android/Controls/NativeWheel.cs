﻿using System;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using eCommerce.Helpers;

namespace eCommerce.Droid.Controls
{
    public class NativeWheel : ImageView
    {
        private Bitmap _originalImage;
        private Matrix _matrix; 
        private double _startAngle;
        private float _spinAmount;

        public Action<float> UpdateSpinAmount;

        protected NativeWheel(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public NativeWheel(Context context, int width, int height, string imageSource) : base(context)
        {
            if (_originalImage == null)
            {
                _originalImage = BitmapFactory.DecodeResource(Resources, Resources.GetIdentifier(imageSource, "drawable", context.PackageName));
            }

            if (_matrix == null)
            {
                _matrix = new Matrix();
            }
            else
            {
                _matrix.Reset();
            }

            SetScaleType(ScaleType.Matrix);

            InitWheel(width, height);

            Touch += WhealView_Touch;
        }

        private void InitWheel(int width, int height)
        {
            Matrix resizeMatrix = new Matrix();
            resizeMatrix.PostScale(Math.Min(width, height) / (float)_originalImage.Width, Math.Min(width, height) / (float)_originalImage.Height);
            var scaledImage = Bitmap.CreateBitmap(_originalImage, 0, 0, _originalImage.Width, _originalImage.Height, resizeMatrix, false);

            var translateX = width / 2 - scaledImage.Width / 2;
            var translateY = height / 2 - scaledImage.Height / 2;
            _matrix.PostTranslate(translateX, translateY);

            SetImageBitmap(scaledImage);
            ImageMatrix = _matrix;
        }

        private void WhealView_Touch(object sender, TouchEventArgs e)
        {
            switch (e.Event.Action)
            {
                case MotionEventActions.Down:
                    var startX = e.Event.GetX() - PivotX;
                    var startY = e.Event.GetY() - PivotY;
                    _startAngle = Math.Atan2(startY, startX);

                    break;
                case MotionEventActions.Move:
                    var currentX = e.Event.GetX() - PivotX;
                    var currentY = e.Event.GetY() - PivotY;

                    var currentAngle = Math.Atan2(currentY, currentX);
                    var angleDifference = _startAngle - currentAngle;
                    var degrees = (float)MathHelper.RadianToDegree(angleDifference);

                    RotateWheel(-degrees);
                    SetSpinAmount(-degrees);
                    _startAngle = currentAngle;

                    break;
            }
        }

        private void RotateWheel(float degrees)
        {
            _matrix.PostRotate(degrees, PivotX, PivotY);
            ImageMatrix = _matrix;
        }

        private void SetSpinAmount(float angleDifference)
        {
            var spinAmount = _spinAmount + angleDifference;

            //HACK: Prevent degree difference between 0 and 360 degree increment and decrement to ensure valid spin amount update.             
            if (Math.Abs(angleDifference) > 300)
            {
                angleDifference = 360 - Math.Abs(angleDifference);

                if (_spinAmount > 0 && _spinAmount < 10)
                {
                    angleDifference = 0;
                }
            }

            if (spinAmount > 0)
            {
                _spinAmount += angleDifference;
            }

            UpdateSpinAmount?.Invoke(_spinAmount);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            Touch -= WhealView_Touch;
        }
    }
}