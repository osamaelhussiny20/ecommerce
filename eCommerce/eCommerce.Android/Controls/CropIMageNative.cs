﻿using System;
using System.IO;
using Android.Content;
using Android.Graphics;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using static Android.Views.ScaleGestureDetector;

namespace eCommerce.Droid.Controls
{
    public class CropImageNative : ImageView
    {
        private ScaleGestureDetector _scaleDetector;
        private Bitmap _originalImage;
        private Bitmap _scaledImage;
        private Matrix _matrix;
        private int _viewHeight;
        private int _viewWidth;
        public float _scaleFactor = 1.0f;
        private float _maxScale = 2.0f;
        private float[] _matrixValues = new float[9];
        private float _minScale = 0.5f;
        private float _previousMoveX;
        private float _previousMoveY;
        private int _intrinsicWidth;
        private int _intrinsicHeight;
        private float _previousDistance;
        private Context CurrentContext;
        private string CurrentImagePath { get; set; }

        public bool IsInitialized { get; set; }
        public bool IsScaling { get; set; }

        public float Scale
        {
            get { return GetValue(_matrix, Matrix.MscaleX); }
        }
        public float TranslateX
        {
            get { return this.GetValue(_matrix, Matrix.MtransX); }
        }
        public float TranslateY
        {
            get { return this.GetValue(_matrix, Matrix.MtransY); }
        }

        private float GetValue(Matrix matrix, int whichValue)
        {
            matrix.GetValues(_matrixValues);
            return _matrixValues[whichValue];
        }

        protected CropImageNative(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public CropImageNative(Context context, string imageSource) : base(context)
        {
            CurrentContext = context;
            CurrentImagePath = imageSource;

            ViewTreeObserver.GlobalLayout += ViewTreeObserver_GlobalLayout;
            Touch += OnTouch;
            _scaleDetector = new ScaleGestureDetector(CurrentContext, new ScaleGestureRecognizer(this));
        }

        private void ViewTreeObserver_GlobalLayout(object sender, EventArgs e)
        {
            if (!IsInitialized)
            {
                IsInitialized = true;
                InitializeNewImage(CurrentImagePath);
            }
        }

        public void InitializeNewImage(string imageSource)
        {
            if (!IsInitialized || string.IsNullOrEmpty(imageSource))
            {
                return;
            }
            if (_originalImage == null)
            {
                _originalImage = BitmapFactory.DecodeFile(imageSource);
                _intrinsicWidth = _originalImage.Width;
                _intrinsicHeight = _originalImage.Height;
            }

            if (_matrix == null)
            {
                _matrix = new Matrix();
            }
            else
            {
                _matrix.Reset();
            }

            SetScaleType(ScaleType.Matrix);

            if (_viewHeight == 0 || _viewWidth == 0)
            {
                _viewHeight = Height;
                _viewWidth = Width;

                _scaledImage = GetScaledBitmap(_originalImage);

                var translateX = _viewWidth / 2 - _scaledImage.Width / 2;
                var translateY = _viewHeight / 2 - _scaledImage.Height / 2;
                _matrix.PostTranslate(translateX, translateY);

                SetImageBitmap(_scaledImage);
                ImageMatrix = _matrix;
            }
        }

        private Bitmap GetScaledBitmap(Bitmap originalImage)
        {
            float scaleFactor = 1.0F;

            var hScaleFactor = (Math.Max((float)Height, originalImage.Height) / Math.Min((float)Height, originalImage.Height));
            var wScaleFactor = (Math.Max((float)Width, originalImage.Width) / Math.Min((float)Width, originalImage.Width));
            scaleFactor = Math.Max(hScaleFactor, wScaleFactor);

            if (originalImage.Width < Width || originalImage.Height < Height)
                scaleFactor = 1;

            var scaledBitmap = Bitmap.CreateScaledBitmap(originalImage, (int)(originalImage.Width / scaleFactor),
                (int)(originalImage.Height / scaleFactor), true);

            return scaledBitmap;
        }
        
        private float Distance(float x0, float x1, float y0, float y1)
        {
            var x = x0 - x1;
            var y = y0 - y1;
            return FloatMath.Sqrt(x * x + y * y);
        }
        private float DispDistance()
        {
            return FloatMath.Sqrt(_viewWidth * _viewWidth + _viewHeight * _viewHeight);
        }

        float dx;
        float dy;
        private void OnTouch(object sender, TouchEventArgs e)
        {
            var touchCount = e.Event.PointerCount;

            switch (e.Event.Action)
            {
                case MotionEventActions.Pointer1Down:
                case MotionEventActions.Pointer2Down:
                    if (touchCount >= 2)
                    {
                        var distance = this.Distance(e.Event.GetX(0), e.Event.GetX(1), e.Event.GetY(0), e.Event.GetY(1));
                        _previousDistance = distance;
                        IsScaling = true;
                    }
                    break;

                case MotionEventActions.Down:
                    _previousMoveX = (int)e.Event.GetX();
                    _previousMoveY = (int)e.Event.GetY();
                    break;

                case MotionEventActions.Move:

                    if (touchCount >= 2 && IsScaling)
                    {
                        var distance = this.Distance(e.Event.GetX(0), e.Event.GetX(1), e.Event.GetY(0), e.Event.GetY(1));
                        var scale = (distance - _previousDistance) / this.DispDistance();
                        _previousDistance = distance;
                        scale += 1;
                        scale = scale * scale;
                        this.ZoomTo(scale, _viewWidth / 2, _viewHeight / 2);
                        _scaleFactor = scale;
                    }
                    else if (!IsScaling)
                    {
                        var distanceX = _previousMoveX - (int)e.Event.GetX();
                        var distanceY = _previousMoveY - (int)e.Event.GetY();
                        _previousMoveX = (int)e.Event.GetX();
                        _previousMoveY = (int)e.Event.GetY();
                        _matrix.PostTranslate(-distanceX, -distanceY);
                        ImageMatrix = _matrix;
                    }
                    break;

                case MotionEventActions.Up:
                case MotionEventActions.Pointer1Up:
                case MotionEventActions.Pointer2Up:
                    if (touchCount <= 1)
                    {
                        IsScaling = false;
                    }
                    break;
            }
        }

        public byte[] GetCroppedPicture()
        {
            var rectLenght = Width - Utils.ConvertDpToPixel(CurrentContext, 40);
            var rectXPos = (Width / 2) - (rectLenght / 2);
            var rectYPos = (Height / 2) - (rectLenght / 2);

            var xPos = TranslateX > 0 ? rectXPos - Math.Abs(TranslateX) : rectXPos + Math.Abs(TranslateX);
            var yPos = TranslateY > 0 ? rectYPos - Math.Abs(TranslateY) : rectYPos + Math.Abs(TranslateY);

            var imgHeight = rectLenght;
            var imgWidth = rectLenght;
            Bitmap croppedBitmap = Bitmap.CreateBitmap(_scaledImage, (int)(xPos/Scale), (int)(yPos/Scale), (int)(imgWidth / Scale), (int)(imgHeight / Scale));
            var matr = _matrix;

            byte[] bitmapData;
            using (var stream = new MemoryStream()) {
                croppedBitmap.Compress(Bitmap.CompressFormat.Jpeg, 100, stream);
                bitmapData = stream.ToArray();
            }

            return bitmapData;
        }

        public void ZoomTo(float scale, int x, int y)
        {
            if (Scale * scale < _minScale)
            {
                scale = _minScale / Scale;
            }
            else
            {
                if (scale >= 1 && Scale * scale > _maxScale)
                {
                    scale = _maxScale / Scale;
                }
            }
            _matrix.PostScale(scale, scale);
            //move to center
            _matrix.PostTranslate(-(_viewWidth * scale - _viewWidth) / 2, -(_viewHeight * scale - _viewHeight) / 2);
            //move x and y distance
            _matrix.PostTranslate(-(x - (_viewWidth / 2)) * scale, 0);
            _matrix.PostTranslate(0, -(y - (_viewHeight / 2)) * scale);
            ImageMatrix = _matrix;
        }        

        private class ScaleGestureRecognizer : Java.Lang.Object, IOnScaleGestureListener
        {
            public IntPtr Handle { get; set; }
            private CropImageNative _view;

            public ScaleGestureRecognizer() { }

            public ScaleGestureRecognizer(CropImageNative view)
            {
                _view = view;
            }

            public bool OnScale(ScaleGestureDetector detector)
            {
                _view._scaleFactor *= detector.ScaleFactor;
                
                var scaleFactor = Math.Max(0.5f, Math.Min(_view._scaleFactor, 5.0f));
                _view._scaleFactor = scaleFactor;
                _view.ZoomTo(scaleFactor, (int)detector.CurrentSpanX, (int)detector.CurrentSpanY);

                return true;
            }

            public bool OnScaleBegin(ScaleGestureDetector detector)
            {
                return true;
            }

            public void OnScaleEnd(ScaleGestureDetector detector)
            {
                return;
            }
        }
    }

}