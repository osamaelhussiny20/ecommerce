﻿using Android.Content;
using Android.Graphics;
using Android.Views;
using eCommerce.Controls;
using eCommerce.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ImageButton), typeof(ImageButtonRenderer))]
namespace eCommerce.Droid.Renderers
{
    public class ImageButtonRenderer : ImageRenderer
    {
        private ImageButton _imageButton;
        private Rect _rect;

        public ImageButtonRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Image> e)
        {
            base.OnElementChanged(e);

            _imageButton = Element as ImageButton;

            if (_imageButton != null)
            {
                _imageButton.HorizontalOptions = LayoutOptions.Center;
                _imageButton.VerticalOptions = LayoutOptions.Center;
            }

            if (Control != null)
            {
                Control.Touch += Control_Touch;
            }
        }

        private void Control_Touch(object sender, TouchEventArgs e)
        {
            switch (e.Event.Action)
            {
                case MotionEventActions.Down:

                    _imageButton?.OnButtonPressed();
                    _rect = new Rect(Left, Top, Right, Bottom);

                    break;
                case MotionEventActions.Up:

                    if (_rect.Contains(Left + (int)e.Event.GetX(), Top + (int)e.Event.GetY()))
                    {
                        _imageButton?.OnButtonReleased();
                    }

                    break;
                case MotionEventActions.Move:

                    if (!_rect.Contains(Left + (int)e.Event.GetX(), Top + (int)e.Event.GetY()))
                    {
                        _imageButton.OnMovedOutside();
                    }

                    break;
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (Control != null)
            {
                Control.Touch -= Control_Touch;
            }
        }
    }
}