﻿using Android.Content;
using System.Threading.Tasks;

using eCommerce.Droid.Controls;
using eCommerce.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using eCommerce.Controls;

[assembly: ExportRenderer(typeof(SwipeButton), typeof(SwipeButtonRenderer))]
namespace eCommerce.Droid.Renderers
{
    public class SwipeButtonRenderer : ViewRenderer<SwipeButton, NativeSwipeButton>
    {
        private SwipeButton _swipeButton;
        private NativeSwipeButton _nativeSwipeButton;

        public SwipeButtonRenderer(Context context) : base(context)
        {

        }

        protected override void OnElementChanged(ElementChangedEventArgs<SwipeButton> e)
        {
            base.OnElementChanged(e);

            _swipeButton = Element as SwipeButton;

            _nativeSwipeButton = new NativeSwipeButton(Context, (int)_swipeButton.HeightRequest, _swipeButton.DragButtonSize, _swipeButton.Text, _swipeButton.Description,
                _swipeButton.IconSource, _swipeButton.BackgroundColor.ToAndroid(), _swipeButton.DragButtonBackgroundColor.ToAndroid(), _swipeButton.TextColor.ToAndroid(),
                _swipeButton.FontFamily, _swipeButton.FontSize, _swipeButton.DescriptionFontSize);

            _swipeButton.BackgroundColor = Color.Transparent;

            SetNativeControl(_nativeSwipeButton);

            _nativeSwipeButton.SwipeComplited = SwipeComplited;
        }

        private async void SwipeComplited()
        {
            var swipeCompletionSource = new TaskCompletionSource<bool>();
            _swipeButton.SwipeComplitedCommand?.Execute(swipeCompletionSource);

            var commandComplited = await swipeCompletionSource.Task;

            if (!commandComplited)
            {
                _nativeSwipeButton.ResetButton();
            }
        }
    }
}