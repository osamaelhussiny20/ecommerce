﻿using Android.Content;
using Android.Support.V4.Content;
using Android.Support.V4.Widget;
using Android.Support.V7.Widget;
using Android.Views;
using Android.App;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms.Platform.Android.AppCompat;
using eCommerce.Droid.Renderers;
using BaseMvvmToolkit.Pages;
using BaseMvvmToolkit.ViewModels;

[assembly: ExportRenderer(typeof(BaseNavigationPage), typeof(BaseNavigationPageRenderer))]

namespace eCommerce.Droid.Renderers
{
    public class BaseNavigationPageRenderer : NavigationPageRenderer
    {
        private Toolbar _toolbar;
        private DrawerLayout _drawerLayout;

        public BaseNavigationPageRenderer(Context context) : base(context)
        {

        }

        protected override void OnLayout(bool changed, int l, int t, int r, int b)
        {
            base.OnLayout(changed, l, t, r, b);

            var basePage = Element.CurrentPage as BasePage;
            var bindingContext = basePage.BindingContext as BaseViewModel;

            if (bindingContext?.DrawerMenuViewModelType != null)
            {
                return;
            }

            if (_toolbar?.NavigationIcon != null && !string.IsNullOrEmpty(basePage?.BackButtonIcon))
            {
                var drawable = Resources.GetIdentifier(basePage.BackButtonIcon, "drawable", Context.PackageName);
                _toolbar.NavigationIcon = ContextCompat.GetDrawable(Context, drawable);
            }
        }

        protected override void OnAttachedToWindow()
        {
            base.OnAttachedToWindow();

            var parent = Element.Parent;
            var context = (Activity)Context;

            if (parent is MasterDetailPage masterDetailPage)
            {
                _drawerLayout = Platform.GetRenderer(masterDetailPage) as MasterDetailPageRenderer;
            }

            _toolbar = context.FindViewById<Toolbar>(Resource.Id.toolbar);

            if (_toolbar?.NavigationIcon != null)
            {
                _toolbar.NavigationClick += ToolbarNavigationClick;
            }
        }

        protected override void OnDetachedFromWindow()
        {
            base.OnDetachedFromWindow();

            if (_toolbar != null)
            {
                _toolbar.NavigationClick -= ToolbarNavigationClick;
            }
        }

        private void ToolbarNavigationClick(object sender, Toolbar.NavigationClickEventArgs e)
        {
            if (Element.Navigation.NavigationStack.Count <= 1 && _drawerLayout != null)
            {
                _drawerLayout.OpenDrawer((int)GravityFlags.Left);
            }

            if (Element.CurrentPage is BasePage basePage)
            {
                if (basePage.BackButtonCommand != null && basePage.BackButtonCommand.CanExecute(null))
                {
                    basePage.BackButtonCommand.Execute(null);
                }
                else
                {
                    Element.PopAsync();
                }
            }
        }
    }
}