﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms.Platform.Android;

using eCommerce.Droid.Renderers;
using Android.Graphics.Drawables;
using eCommerce.Controls;

[assembly: ExportRenderer(typeof(PlaceholderEditor), typeof(PlaceholderEditorRenderer))]
namespace eCommerce.Droid.Renderers
{
    class PlaceholderEditorRenderer : EditorRenderer
    {
        public PlaceholderEditorRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(
            ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);

            var gradient = new GradientDrawable();
            gradient.SetCornerRadius(15);
            gradient.SetColor(Color.White.ToAndroid());
            Background = gradient;
            var placeholderEditor = Element as PlaceholderEditor;

            gradient.SetStroke(placeholderEditor.BorderThickness, placeholderEditor.BorderColor.ToAndroid());
            gradient.SetCornerRadius(Utils.ConvertPixelToDp(Context, placeholderEditor.BorderRadius));
            if (e.NewElement != null)
            {
                var element = e.NewElement as PlaceholderEditor;

                this.Control.Hint = element.Placeholder;
            }
        }
    }

}