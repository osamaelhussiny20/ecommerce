﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using eCommerce.Controls;
using eCommerce.Droid.Controls;
using eCommerce.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Wheel), typeof(WheelRenderer))]

namespace eCommerce.Droid.Renderers
{
    public class WheelRenderer : ViewRenderer<Wheel, WheelLayout>
    {
        private Wheel _wheel;

        public WheelRenderer(Context context) : base(context)
        {
        }

        protected override async void OnElementChanged(ElementChangedEventArgs<Wheel> e)
        {
            base.OnElementChanged(e);

            _wheel = Element as Wheel;

            if (_wheel == null)
            {
                return;
            }

            var profilePicture = _wheel.ProfilePicture;
            var backgroundImage = _wheel.BackgroundImage;

            if (profilePicture != null && !string.IsNullOrEmpty(backgroundImage))
            {
                var bitmap = await BitmapFactory.DecodeByteArrayAsync(profilePicture, 0, profilePicture.Length);
                var wheelLayout = new WheelLayout(Context, (float)_wheel.Width, (float)_wheel.Width, bitmap, backgroundImage);

                var viewContent = wheelLayout.ToView();

                viewContent.HeightRequest = _wheel.Width;
                viewContent.WidthRequest = _wheel.Width;

                _wheel.Content = viewContent;

                wheelLayout.GetSpinAmount(UpdateSpinAmount);
            }
        }

        private void UpdateSpinAmount(float spinAmount)
        {
            _wheel.SpinAmount = (int)spinAmount;
        }
    }

}