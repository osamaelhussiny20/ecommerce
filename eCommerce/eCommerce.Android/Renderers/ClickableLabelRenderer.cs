﻿using Android.Content;
using Android.Graphics;
using Android.Views;
using eCommerce.Controls;
using eCommerce.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ClickableLabel), typeof(ClickableLabelRenderer))]
namespace eCommerce.Droid.Renderers
{
    public class ClickableLabelRenderer : LabelRenderer
    {
        private ClickableLabel _clickableLabel;
        private Rect _rect;

        public ClickableLabelRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            _clickableLabel = Element as ClickableLabel;

            if (Control != null)
            {
                Control.Touch += Control_Touch;
            }
        }

        private void Control_Touch(object sender, TouchEventArgs e)
        {
            switch (e.Event.Action)
            {
                case MotionEventActions.Down:

                    _clickableLabel?.OnPressed();
                    _rect = new Rect(Left, Top, Right, Bottom);

                    break;
                case MotionEventActions.Up:

                    if (_rect.Contains(Left + (int)e.Event.GetX(), Top + (int)e.Event.GetY()))
                    {
                        _clickableLabel?.OnReleased();
                    }

                    break;
                case MotionEventActions.Move:

                    if (!_rect.Contains(Left + (int)e.Event.GetX(), Top + (int)e.Event.GetY()))
                    {
                        _clickableLabel.OnMovedOutside();
                    }

                    break;
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (Control != null)
            {
                Control.Touch -= Control_Touch;
            }
        }
    }
}