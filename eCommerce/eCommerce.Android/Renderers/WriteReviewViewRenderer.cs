﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using eCommerce.Controls;
using eCommerce.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(WriteReviewView), typeof(WriteReviewViewRenderer))]
namespace eCommerce.Droid.Renderers
{
    public class WriteReviewViewRenderer : ViewRenderer
    {
        private WriteReviewView _writeReviewView;

        public WriteReviewViewRenderer(Context context) : base(context)
        {
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.View> e)
        {
            base.OnElementChanged(e);
            _writeReviewView = Element as WriteReviewView;

            if (Control == null)
            {
                var view = new Android.Views.View(Context);
                SetNativeControl(view);
            }

            Control.ViewTreeObserver.GlobalLayout += ViewTreeObserver_GlobalLayout;
        }

    
        private void ViewTreeObserver_GlobalLayout(object sender, EventArgs e)
        {
            var screenHeight = Control?.RootView.Height;
            var rect = new Rect();
            var mainView = MainActivity.Instance.Window.DecorView;

            mainView.GetWindowVisibleDisplayFrame(rect);

            if (screenHeight != null)
            {
                var keyboardHeight = screenHeight - rect.Bottom;
                var keyboardHeightInDp = Utils.ConvertPixelToDp(Context, (float)-keyboardHeight);

                _writeReviewView.TranslationY = keyboardHeightInDp + _writeReviewView.Height;
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (Control != null)
            {
                Control.ViewTreeObserver.GlobalLayout -= ViewTreeObserver_GlobalLayout;
            }
        }
    }
}