﻿using System;
using Android.Content;
using Xamarin.Forms;
using eCommerce.Droid.Renderers;
using Xamarin.Forms.Platform.Android;
using Android.Graphics;
using Android.Views;
using eCommerce.Controls;
using eCommerce.Enums;

[assembly: ExportRenderer(typeof(NoBorderButton), typeof(NoBorderButtonRenderer))]
namespace eCommerce.Droid.Renderers
{
    public class NoBorderButtonRenderer : ButtonRenderer
    {
        private NoBorderButton _newElement;
        private double _spaceModifier;

        public NoBorderButtonRenderer(Context context) : base(context) { }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                _newElement = e.NewElement as NoBorderButton;

                if (_newElement != null)
                {
                    if (_newElement.DisplayAboveKeyboard)
                    {
                       // Control.ViewTreeObserver.GlobalLayout += ViewTreeObserver_GlobalLayout;
                    }

                    if (_newElement.TextAligment != ButtonTextAlignment.Default)
                    {
                        AlignText();
                    }
                }
            }
        }

        private void ViewTreeObserver_GlobalLayout(object sender, EventArgs e)
        {
            var screenHeight = Control?.RootView.Height;
            var rect = new Rect();
            var mainView = MainActivity.Instance.Window.DecorView;

            mainView.GetWindowVisibleDisplayFrame(rect);

            if (screenHeight != null)
            {
                var keyboardHeight = screenHeight - rect.Bottom;
                var keyboardHeightInDp = Utils.ConvertPixelToDp(Context, (float)-keyboardHeight);

                if (_newElement.TranslationY > 0)
                {
                    _spaceModifier = _newElement.TranslationY;
                }

                _newElement.TranslationY = keyboardHeightInDp + _newElement.Height - _spaceModifier;
            }
        }

        private void AlignText()
        {
            switch (_newElement.TextAligment)
            {
                case ButtonTextAlignment.Default:
                    break;
                case ButtonTextAlignment.Left:
                    Control.Gravity = (GravityFlags.CenterVertical | GravityFlags.Left);
                    break;
                case ButtonTextAlignment.Center:
                    Control.Gravity = GravityFlags.Center;
                    break;
                case ButtonTextAlignment.Right:
                    Control.Gravity = (GravityFlags.CenterVertical | GravityFlags.Right);
                    break;
            }
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (Control != null)
            {
                Control.ViewTreeObserver.GlobalLayout -= ViewTreeObserver_GlobalLayout;
            }
        }
    }
}