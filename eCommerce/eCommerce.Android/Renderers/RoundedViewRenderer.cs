﻿using Android.Content;
using Android.Graphics.Drawables;
using eCommerce.Controls;
using eCommerce.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(RoundedView), typeof(RoundedViewRenderer))]
namespace eCommerce.Droid.Renderers
{
    public class RoundedViewRenderer : ViewRenderer<RoundedView, Android.Views.View>
    {
        public RoundedViewRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<RoundedView> e)
        {
            base.OnElementChanged(e);

            var gradient = new GradientDrawable();

            if (Element is RoundedView roundedView)
            {
                gradient.SetStroke(roundedView.BorderThickness, roundedView.BorderColor.ToAndroid());
                gradient.SetCornerRadius(roundedView.BorderRadius);
                gradient.SetColor(roundedView.BackgroundColor.ToAndroid());
            }

            Background = gradient;
        }
    }
}