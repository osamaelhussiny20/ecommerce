﻿using Android.Content;
using eCommerce.Droid.Renderers;

using eCommerce.Droid.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using System;
using eCommerce.Controls;
using eCommerce.Droid.Controls;

[assembly: ExportRenderer(typeof(CropImageControl), typeof(ImageCropRenderer))]
namespace eCommerce.Droid.Renderers
{
    public class ImageCropRenderer : ViewRenderer<CropImageControl, CropImageLayout>
    {
        private CropImageControl _cropImageControl;
        private CropImageLayout _cropImageLayout;

        public ImageCropRenderer(Context context) : base(context) { }        

        protected override void OnElementChanged(ElementChangedEventArgs<CropImageControl> e)
        {

            _cropImageControl = Element as CropImageControl;

            _cropImageLayout = new CropImageLayout(Context, _cropImageControl.ImageSource);
            _cropImageControl.ImageSourceAddedCommand = new Command(ImageSourceAddedExecute);
            _cropImageControl.GetCroppedImageCommand = new Command(GetCroppedImageCommandExecute);

            SetNativeControl(_cropImageLayout);

            base.OnElementChanged(e);
        }

        private void GetCroppedImageCommandExecute(object obj)
        {
            var data = _cropImageLayout.GetCroppedImage();
            _cropImageControl.ImageCroppedCommand?.Execute(data);
        }

        private void ImageSourceAddedExecute(object obj)
        {
            var path = obj as string;
            if (path != null)
            {
                _cropImageLayout.InitializeImageControl(path);
            }
        }
    }
}