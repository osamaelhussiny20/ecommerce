package md5d81a674d3f91bd6c33cd86ee881b48c6;


public class CropImageNative_ScaleGestureRecognizer
	extends java.lang.Object
	implements
		mono.android.IGCUserPeer,
		android.view.ScaleGestureDetector.OnScaleGestureListener
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onScale:(Landroid/view/ScaleGestureDetector;)Z:GetOnScale_Landroid_view_ScaleGestureDetector_Handler:Android.Views.ScaleGestureDetector/IOnScaleGestureListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"n_onScaleBegin:(Landroid/view/ScaleGestureDetector;)Z:GetOnScaleBegin_Landroid_view_ScaleGestureDetector_Handler:Android.Views.ScaleGestureDetector/IOnScaleGestureListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"n_onScaleEnd:(Landroid/view/ScaleGestureDetector;)V:GetOnScaleEnd_Landroid_view_ScaleGestureDetector_Handler:Android.Views.ScaleGestureDetector/IOnScaleGestureListenerInvoker, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null\n" +
			"";
		mono.android.Runtime.register ("eCommerce.Droid.Controls.CropImageNative+ScaleGestureRecognizer, eCommerce.Android", CropImageNative_ScaleGestureRecognizer.class, __md_methods);
	}


	public CropImageNative_ScaleGestureRecognizer ()
	{
		super ();
		if (getClass () == CropImageNative_ScaleGestureRecognizer.class)
			mono.android.TypeManager.Activate ("eCommerce.Droid.Controls.CropImageNative+ScaleGestureRecognizer, eCommerce.Android", "", this, new java.lang.Object[] {  });
	}

	public CropImageNative_ScaleGestureRecognizer (md5d81a674d3f91bd6c33cd86ee881b48c6.CropImageNative p0)
	{
		super ();
		if (getClass () == CropImageNative_ScaleGestureRecognizer.class)
			mono.android.TypeManager.Activate ("eCommerce.Droid.Controls.CropImageNative+ScaleGestureRecognizer, eCommerce.Android", "eCommerce.Droid.Controls.CropImageNative, eCommerce.Android", this, new java.lang.Object[] { p0 });
	}


	public boolean onScale (android.view.ScaleGestureDetector p0)
	{
		return n_onScale (p0);
	}

	private native boolean n_onScale (android.view.ScaleGestureDetector p0);


	public boolean onScaleBegin (android.view.ScaleGestureDetector p0)
	{
		return n_onScaleBegin (p0);
	}

	private native boolean n_onScaleBegin (android.view.ScaleGestureDetector p0);


	public void onScaleEnd (android.view.ScaleGestureDetector p0)
	{
		n_onScaleEnd (p0);
	}

	private native void n_onScaleEnd (android.view.ScaleGestureDetector p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
