package md5d81a674d3f91bd6c33cd86ee881b48c6;


public class NativeWheel
	extends android.widget.ImageView
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("eCommerce.Droid.Controls.NativeWheel, eCommerce.Android", NativeWheel.class, __md_methods);
	}


	public NativeWheel (android.content.Context p0)
	{
		super (p0);
		if (getClass () == NativeWheel.class)
			mono.android.TypeManager.Activate ("eCommerce.Droid.Controls.NativeWheel, eCommerce.Android", "Android.Content.Context, Mono.Android", this, new java.lang.Object[] { p0 });
	}


	public NativeWheel (android.content.Context p0, android.util.AttributeSet p1)
	{
		super (p0, p1);
		if (getClass () == NativeWheel.class)
			mono.android.TypeManager.Activate ("eCommerce.Droid.Controls.NativeWheel, eCommerce.Android", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android", this, new java.lang.Object[] { p0, p1 });
	}


	public NativeWheel (android.content.Context p0, android.util.AttributeSet p1, int p2)
	{
		super (p0, p1, p2);
		if (getClass () == NativeWheel.class)
			mono.android.TypeManager.Activate ("eCommerce.Droid.Controls.NativeWheel, eCommerce.Android", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android:System.Int32, mscorlib", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public NativeWheel (android.content.Context p0, android.util.AttributeSet p1, int p2, int p3)
	{
		super (p0, p1, p2, p3);
		if (getClass () == NativeWheel.class)
			mono.android.TypeManager.Activate ("eCommerce.Droid.Controls.NativeWheel, eCommerce.Android", "Android.Content.Context, Mono.Android:Android.Util.IAttributeSet, Mono.Android:System.Int32, mscorlib:System.Int32, mscorlib", this, new java.lang.Object[] { p0, p1, p2, p3 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
