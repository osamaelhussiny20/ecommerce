﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace eCommerce.Converters
{
    public class CountToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Color resultColor = Color.Default;

            if(value is Int32)
            {
                if((int)value > 0)
                {
                    resultColor = Color.LightGreen;
                }
                else if((int)value < 0)
                {
                    resultColor = Color.Red;
                }
                else
                {
                    resultColor = Color.LightGray;
                }
            }

            return resultColor;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
