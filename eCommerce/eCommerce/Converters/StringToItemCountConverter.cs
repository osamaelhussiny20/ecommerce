﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace eCommerce.Converters
{
    public class StringToItemCountConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var itemCount = string.Empty;
            if (value != null)
            {
                itemCount = value.ToString();
                if (!string.IsNullOrEmpty(itemCount))
                {
                    itemCount = Regex.Replace(itemCount, ".{2}", "$0 ");
                }
            }

            return itemCount;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (string)value.ToString().Replace(" ", string.Empty);
        }
    }
}
