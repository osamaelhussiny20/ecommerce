﻿using System;
using System.Globalization;
using eCommerce.Enums;
using Xamarin.Forms;

namespace eCommerce.Converters
{
    public class PinCodeEntryModeToStringConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string text = string.Empty;

            if (value is PinCodeEntryMode)
            {
                //switch ((PinCodeEntryMode)value)
                //{
                //    case PinCodeEntryMode.Enter:
                //        text = AppResources.EnterPersonalCodeLabel;
                //        break;
                //    case PinCodeEntryMode.EnterOld:
                //        text = AppResources.PinCodeEntryMode_EnterOldText;
                //        break;
                //    case PinCodeEntryMode.EnterNew:
                //        text = AppResources.PinCodeEntryMode_EnterNewText;
                //        break;
                //    case PinCodeEntryMode.Choose:
                //        text = AppResources.PinCodeEntryMode_ChooseText;
                //        break;
                //    case PinCodeEntryMode.Verify:
                //        text = AppResources.PinCodeEntryMode_VerifyText;
                //        break;
                //    default:
                //        text = AppResources.EnterPersonalCodeLabel;
                //        break;
                //}
            }

            return text;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
