﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace eCommerce.Converters
{
    public class StringToPhoneNumberConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var phoneNumber = string.Empty;
            if (value != null)
            {
                phoneNumber = value.ToString();
                if (!string.IsNullOrEmpty(phoneNumber))
                {
                    phoneNumber = Regex.Replace(phoneNumber, ".{2}", "$0 ");
                }
            }

            return phoneNumber;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (string)value.ToString().Replace(" ", string.Empty);
        }
    }
}
