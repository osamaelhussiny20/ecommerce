﻿using System;
using System.Globalization;
using eCommerce.Enums;
using Xamarin.Forms;

namespace eCommerce.Converters
{
    public class NavigationTypeToImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string imageSource = string.Empty;

            if (value is NavigationButtonType)
            {
                switch ((NavigationButtonType)value)
                {
                    case NavigationButtonType.Back:
                        imageSource = "left_arrow";
                        break;
                    case NavigationButtonType.Close:
                        imageSource = "cross";
                        break;
                }
            }

            return imageSource;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }
    }
}
