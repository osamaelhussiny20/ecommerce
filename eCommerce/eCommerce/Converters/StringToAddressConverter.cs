﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace eCommerce.Converters
{
    public class StringToAddressConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var address = value as string;
            var splitsAddress = address?.Split(',');

            if(splitsAddress?.Length > 0)
            {
                return $"{splitsAddress[0]}";
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
