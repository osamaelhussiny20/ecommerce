﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace eCommerce.Converters
{
    public class StringToShortDateTimeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var phoneNumber = string.Empty;
            if (value != null)
            {
                phoneNumber = value.ToString();
                if(phoneNumber.Length >2 && !phoneNumber.Contains("/"))
                {
                    phoneNumber = phoneNumber.Insert(2, "/");
                }
            }

            return phoneNumber;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (string)value.ToString();
        }
    }
}
