﻿using Autofac;
using BaseMvvmToolkit.Services;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace eCommerce.Behaviors
{
    public class EntryClickBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry bindable)
        {
            base.OnAttachedTo(bindable);
            bindable.Focused += Bindable_Focused;
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.Focused -= Bindable_Focused;
        }

        private void Bindable_Focused(object sender, FocusEventArgs e)
        {
            var app = Application.Current as App;
         //   var navigationService = app?.Container.Resolve<INavigationService>();
        }
    }
}
