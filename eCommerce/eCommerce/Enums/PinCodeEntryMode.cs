﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerce.Enums
{
    public enum PinCodeEntryMode
    {
        Enter,
        EnterOld,
        EnterNew,
        Choose,
        Verify
    }
}
