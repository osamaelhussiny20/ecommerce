﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerce.Enums
{
    public enum MediaType
    {
        UserProfileImage,
        CompanyProfileImage
    }
}
