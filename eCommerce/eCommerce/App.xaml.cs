﻿using Autofac;
using BaseMvvmToolkit.Extensions;
using BaseMvvmToolkit.Services;
using eCommerce.Helpers;
using eCommerce.Services;
using eCommerce.Services.CloudStorageManagerService;
using eCommerce.Services.UserDialog;
using eCommerce.ViewModels;
using eCommerce.ViewModels.CountrySelectorList;
using Microsoft.AppCenter;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Microsoft.AppCenter.Push;
using System;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace eCommerce
{
    public static class AppContainer
    {
        public static IContainer Container { get; set; }
    }
    public partial class App : Application
    {
        public IContainer Container { get; private set; }

        public App()
        {
            InitializeComponent();
            SetupAppCenter();
            SetupDependencyInjection();
            SetStartPage();
            FetchFiles();
        }
        private async void FetchFiles()
        {
           // await Helpers.CloudStorageManager.GetAppDataFile(CloudAppDataFiles.CountriesJson);
        }

        private void SetStartPage()
        {
            var navigationService = Container.Resolve<INavigationService>();
            if (GeneralSettings.CanFastLogin && GeneralSettings.GetActiveUser().Id != Guid.Empty.ToString())
                navigationService.SetMainViewModel<CountrySelectorListViewModel>(GeneralSettings.GetActiveUser().Id);
            else
                navigationService.SetMainViewModel<MainViewModel>();
            // navigationService.SetMainViewModel<TermsAndConditionsViewModel>();
        }

        private void SetupDependencyInjection()
        {
            if (Container != null)
            {
                return;
            }

            var builder = new ContainerBuilder();

            builder.RegisterMvvmComponents(typeof(App).GetTypeInfo().Assembly);
            builder.RegisterType<NavigationService>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterXamDependency<IToastService>();
           
            builder.RegisterType<MediaPickerService>().As<IMediaPickerService>();
            builder.RegisterType<CloudStorageManagerService>().As<ICloudStorageManagerService>();
          
            //builder.RegisterRestApi<IeCommerceLiveAPI>();

            Container = builder.Build();
            AppContainer.Container = Container;

        }


        private void SetupAppCenter()
        {
            AppCenter.Start("android=6bb3a1eb-2929-4977-8e58-03af58183c3c;" + "ios=6a9b1fb2-1832-40f2-aeca-7f2e632155d8;", typeof(Analytics), typeof(Crashes), typeof(Push));
        }
        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
