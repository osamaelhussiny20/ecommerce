﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using BaseMvvmToolkit.Services;
using BaseMvvmToolkit.ViewModels;
using eCommerce.Services.UserDialog;
using Xamarin.Forms;

namespace eCommerce.ViewModels.Menu
{
    public  class MenuMasterViewModel: BaseViewModel
    {
       // private IUserService _userService;
        private IToastService _ToastService;
        public MenuMasterViewModel(INavigationService navigationService,  IToastService toastService) : base(navigationService)
        {
          
            _ToastService = toastService;
            Title = "Fatma";
            
            if (Device.RuntimePlatform == Device.iOS)
                Icon = "hamburger.png";

            PopulateMenu();

        }
#region Commands
        public ICommand GoToStart
        {
            get
            {
                return new Command(() =>
                {
                    
                    NavigationService.NavigateToMenuItem<MainViewModel>();
                });
            }
        }
        public ICommand GoToSearchEvent
        {
            get
            {
                return new Command(() =>
                {

                  //  NavigationService.NavigateToMenuItem<SearchEventViewModel>();
                });
            }
        }
        public ICommand GoToSaveEvent
        {
            get
            {
                return  new Command(() =>
                {
                //    NavigationService.NavigateToMenuItem<SavedEventViewModel>();
                });
            }

        }

        public ICommand GoToTeamManger
        {
            get
            {
                return new Command(() =>
                {
                 //   NavigationService.NavigateToMenuItem<TeamMangerViewModel>();
                });
            }

        }
        public ICommand LogBook
        {
            get
            {
                return new Command(() =>
                {
                    NavigationService.NavigateToMenuItem<MainViewModel>();
                });
            }

        }
        public ICommand GoToMyEvent
        {
            get
            {
                return new Command(() =>
                {
                  //  NavigationService.NavigateToMenuItem<YourEventViewModel>();
                });
            }

        }
        public ICommand OpenHelpItem
        {
            get
            {
                return new Command(() =>
                {
                    NavigationService.NavigateToMenuItem<MainViewModel>();
                });
            }

        }

#endregion
        public ObservableCollection<MenuItem> MenuItems { get; set; }
        public MenuItem SelectedMenuItem
        {
            get { return null; }
            set
            {
                if (value != null && value.IsEnabled && value.Command != null)
                {
                    NavigationService.CloseDrawerMenu();
                    value.Command.Execute(null);
                }
            }
        }

        

        void PopulateMenu()
        {
            
            var searchEventMenuItem = new MenuItem
            {
                Command = GoToSearchEvent,
                Title = "SearchEvent"
            };
            var SavedEventMenuItem = new MenuItem
            {
                Command = GoToSaveEvent,
                Title = "Saved Event"
            };
            var TeamMangerMenuItem = new MenuItem
            {
               Command = GoToTeamManger,

                Title = "TeamManger",

            };
           


           
            var MyEventMenuItem = new MenuItem
            {
                Command = GoToMyEvent,
                Title = "My Event "
            };

            var helpMenuItem = new MenuItem
            {
                Command = OpenHelpItem,
                Title = "ABOUT"
            };

        

            MenuItems = new ObservableCollection<MenuItem>
            {
               
                searchEventMenuItem,
                SavedEventMenuItem,
                TeamMangerMenuItem,
                MyEventMenuItem,
                helpMenuItem

            };
        }

        //private async void FetchUserInfo()
        //{
        //    if (CrossConnectivity.Current.IsConnected)
        //    {
        //        var userInfo = await _userService.GetUserInfo(new GetUserInfoRequest
        //        {
        //            UserID = Guid.Parse(GeneralSettings.ActiveUserId),
        //            CompanyId = Guid.Parse(GeneralSettings.CompanyId)
        //        });
        //        GeneralSettings.CompanyId = userInfo.CompanyID;
        //        GeneralSettings.ActiveUserAppUserType = userInfo.UserType;
        //        GeneralSettings.ActiveUserProfilePicture = userInfo.ImagePath;
        //        GeneralSettings.NumberofUnConfirmedCompanies = userInfo.NumberOfNewCompanies.ToString();
        //        GeneralSettings.NumberofConfirmedCompanies = userInfo.numberOfCompanies.ToString();
        //        ActiveFullName = GeneralSettings.ActiveFullName;
        //        CurrentUser = new UserViewModel()
        //        {
        //            ProfileImage = GeneralSettings.ActiveUserProfilePicture,
        //            Name = ActiveFullName[0].ToString().ToUpper() + ActiveFullName.Substring(1),

        //        };
        //        ProfileImage = CurrentUser.ProfileImage;
        //    }
        //    else
        //    {
        //        ProfileImage = GeneralSettings.ActiveUserProfilePicture;
        //        ActiveFullName = GeneralSettings.ActiveFullName;
        //        CurrentUser = new UserViewModel()
        //        {
        //            ProfileImage = GeneralSettings.ActiveUserProfilePicture,
        //            Name = ActiveFullName[0].ToString().ToUpper() + ActiveFullName.Substring(1),

        //        };
        //    }
        //}
    }
}
