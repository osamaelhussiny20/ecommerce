﻿using BaseMvvmToolkit.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;

namespace eCommerce.ViewModels.Menu
{
    [ImplementPropertyChanged]
    public class MenuItem
    {
        public MenuItem()
        {
            IsEnabled = true;
            IsVisible = true;
        }

        public string Icon { get; set; }

        public string Title { get; set; }
        public string NotfiyText { get; set; }

        public ICommand Command { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsVisible { get; set; }
        public bool IsNotfiy { get; set; }

    }
}
