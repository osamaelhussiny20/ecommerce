﻿using System;
using System.Collections.Generic;
using System.Text;
using BaseMvvmToolkit.Services;
using BaseMvvmToolkit.ViewModels;

namespace eCommerce.ViewModels.Menu
{
    public class MenuDetailViewModel: BaseViewModel
    {
        public MenuDetailViewModel(INavigationService navigationService) : base(navigationService)
        {
                
        }
        public override Type DrawerMenuViewModelType => typeof(MenuMasterViewModel);
    }
}
