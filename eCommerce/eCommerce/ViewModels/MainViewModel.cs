﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using BaseMvvmToolkit.Services;
using BaseMvvmToolkit.ViewModels;
using eCommerce.Services.UserDialog;
using eCommerce.ViewModels.Menu;

namespace eCommerce.ViewModels
{
    public class MainViewModel : MenuDetailViewModel
    {
        private readonly IToastService _ToastService;

        public MainViewModel(INavigationService navigationService, IToastService toastService) : base(navigationService)
        {
            Title = "MF";
            _ToastService = toastService;
        }

        public override Task Init(object args)
        {
            return base.Init(args);
        }
    }
}
