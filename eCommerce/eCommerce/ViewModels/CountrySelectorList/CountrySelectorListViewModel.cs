﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows.Input;
using BaseMvvmToolkit.Services;
using BaseMvvmToolkit.ViewModels;
using eCommerce.Helpers;
using eCommerce.Models.Country;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace eCommerce.ViewModels.CountrySelectorList
{
    public class CountrySelectorListViewModel : BaseViewModel
    {
        private CountryModel _selectedCountry;

        public List<CountryModel> CountryList { get; set; }

        public CountryModel SelectedCountry
        {
            get { return _selectedCountry; }
            set
            {
                _selectedCountry = value;
                OnPropertyChanged();
                SelectCountryCommandExecute(_selectedCountry);
            }
        }

        public ICommand CloseButtonCommand { get; }

        public CountrySelectorListViewModel(INavigationService navigationService) : base(navigationService)
        {
            CloseButtonCommand = new Command(CloseButtonCommandExecute);
        }

        public override void OnAppearing()
        {
            base.OnAppearing();
            GetCountryList();
        }

        private void GetCountryList()
        {

            try
            {
                var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                var filePath = Path.Combine(documentsPath, "Countries.json");
                var file = File.ReadAllText(filePath);
                CountryList = JsonConvert.DeserializeObject<CountriesModel>(file).Countries;
            }
            catch (Exception e)
            {
                string error = e.Message;
                throw;
            }
        }

        private async void CloseButtonCommandExecute(object obj)
        {
            await NavigationService.PopModalAsync();
        }

        private async void SelectCountryCommandExecute(CountryModel selectedCountry)
        {
            CountryHelper.SetCurrentCountry(selectedCountry);
            await NavigationService.PopModalAsync();
        }
    }
}
