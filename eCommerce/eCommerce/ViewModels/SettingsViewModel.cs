﻿using BaseMvvmToolkit.Services;
using BaseMvvmToolkit.ViewModels;
using eCommerce.Models;
using eCommerce.Models.User;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;


namespace eCommerce.ViewModels
{
    public class SettingsViewModel :BaseViewModel
    {
        private bool _needRefresh = true;

        public ActiveUser ActiveUser { get; set; }
        public string VersionString { get; set; }
        public GroupedViewModel SelectedSetting { get; set; }
        public ObservableCollection<PageTypeGroup> SettingsItems { get; set; } =
           new ObservableCollection<PageTypeGroup>();

        public ICommand SettingsItemSelectedCommand { get; }

        public SettingsViewModel(INavigationService navigationService) : base(navigationService)
        {
            //Title = AppResources.Menu_Settings;
            //VersionString = AppResources.VersionString;

            SettingsItemSelectedCommand = new Command(args => SettingsItemSelectedCommandExecute(args));
        }

        //private void InitializeSettingsList()
        //{
        //    SettingsItems.Clear();



        //    var generalGroup = new PageTypeGroup(AppResources.SettingsGroupItem_Profile, " ");

        //    //var personalizationGroup = new PageTypeGroup(AppResources.SettingsGroupItem_Personalization, " ");
        //    var aboutGroup = new PageTypeGroup(AppResources.SettingsGroupItem_About, " ");

        //    generalGroup.Add(new GroupedViewModel(AppResources.Settings_MenuItem_Name, GeneralSettings.ActiveFullName, typeof(SettingsFirstNameViewModel)));
        //    generalGroup.Add(new GroupedViewModel(AppResources.Settings_MenuItem_PhoneNumber, GeneralSettings.ActiveUserPhoneNumber, null, null, false));
        //    generalGroup.Add(new GroupedViewModel(AppResources.Settings_MenuItem_Email, GeneralSettings.ActiveUserEmail, typeof(SettingsUserEmailViewModel)));
        //    generalGroup.Add(new GroupedViewModel(AppResources.Settings_MenuItem_ChangePassword, viewModelType: typeof(SettingsRegistrationPasswordViewModel)));
        //    generalGroup.Add(new GroupedViewModel(AppResources.Settings_MenuItem_ChangeProfilePicture, viewModelType: typeof(SettingsChangeProfilePictureViewModel), showAsModal: false));
        //    generalGroup.Add(new GroupedViewModel(AppResources.Settings_MenuItem_Logout, actionHandler: () =>


        //    {
        //        GeneralSettings.SetActiveUserInfoToDefaults();
        //        NavigationService.SetMainViewModel<StartViewModel>();
        //    }));


        //    //personalizationGroup.Add(new GroupedViewModel(AppResources.Settings_MenuItem_Language, CountryHelper.GetCurrentCountry().Name));
        //    //personalizationGroup.Add(new GroupedViewModel(AppResources.Settings_MenuItem_Notifications));


        //    aboutGroup.Add(new GroupedViewModel(AppResources.Settings_MenuItem_Terms, actionHandler: () => Device.OpenUri(new Uri("http://www.bearapps.dk"))));
        //    SettingsItems.Add(generalGroup);

        //    //SettingsItems.Add(personalizationGroup);
        //    SettingsItems.Add(aboutGroup);

        //    _needRefresh = false;
        //}
        private async void SettingsItemSelectedCommandExecute(object args)
        {
            var selectedItem = args as GroupedViewModel;

            if (selectedItem == null)
                return;

            if (selectedItem.ViewModelType != null)
            {
                if (selectedItem.ShowAsModal)
                    await NavigationService.NavigateToModalAsync(selectedItem.ViewModelType);
                else
                    await NavigationService.NavigateToAsync(selectedItem.ViewModelType);
            }
            selectedItem.ActionHandler?.Invoke();
        }

        //public override void OnAppearing()
        //{
        //    if (_needRefresh)
        //    {
        //        InitializeSettingsList();
        //    }

        //    base.OnAppearing();
        //}

        public override Task Init(object args)
        {
            if (args is bool)
            {
                _needRefresh = (bool)args;
            }

            return base.Init(args);
        }
    }
}