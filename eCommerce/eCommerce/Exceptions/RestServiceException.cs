﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerce.Exceptions
{
    public class RestServiceException : Exception
    {
        public override string Message { get; }

        public RestServiceException() { }

        public RestServiceException(string message)
        {
            Message = message;
        }
    }

}
