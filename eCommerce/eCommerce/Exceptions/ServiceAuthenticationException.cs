﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerce.Exceptions
{
    public class ServiceAuthenticationException : Exception
    {
        public string Message { get; }

        public ServiceAuthenticationException() { }

        public ServiceAuthenticationException(string message)
        {
            Message = message;
        }
    }
}
