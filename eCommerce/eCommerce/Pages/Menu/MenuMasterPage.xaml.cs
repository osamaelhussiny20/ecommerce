﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace eCommerce.Pages.Menu
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MenuMasterPage 
	{
		public MenuMasterPage ()
		{
			InitializeComponent ();
		}
	}
}