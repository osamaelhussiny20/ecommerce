﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace eCommerce.Pages.CountrySelectorList
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CountrySelectorListPage 
	{
		public CountrySelectorListPage ()
		{
			InitializeComponent ();
		}
	}
}