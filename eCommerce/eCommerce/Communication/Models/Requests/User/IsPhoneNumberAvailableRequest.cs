﻿using System;
using System.Collections.Generic;
using System.Text;
using Refit;

namespace eCommerce.Communication.Models.Requests.User
{
    public class IsPhoneNumberAvailableRequest : RequestBase
    {
        [AliasAs("phonenumber")]
        public string PhoneNumber { get; set; }
    }
}
