﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerce.Communication.Models.Requests.User
{
    public class UpdateAppCenterParamsRequest : RequestBase
    {
        public string PhoneNumber { get; set; }
        public string DeviceID { get; set; }

    }
}
