﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerce.Communication.Models.Requests.User
{
    public class ConfirmRequest : RequestBase
    {
        public Guid UserID { get; set; }
        public string ActivationCode { get; set; }
    }
}
