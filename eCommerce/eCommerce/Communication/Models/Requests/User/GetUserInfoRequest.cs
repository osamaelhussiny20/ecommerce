﻿using System;
using System.Collections.Generic;
using System.Text;
using Refit;

namespace eCommerce.Communication.Models.Requests.User
{
    public class GetUserInfoRequest : RequestBase
    {
        [AliasAs("UserID")]
        public Guid UserID { get; set; }
        public Guid CompanyId { get; set; }
    }
}
