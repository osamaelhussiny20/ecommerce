﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerce.Communication.Models.Requests.User
{
    public class UpdateUserInformationRequest : RequestBase
    {
        public Guid UserID { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string PassCode { get; set; }
        public string DeviceID { get; set; }
    }
}
