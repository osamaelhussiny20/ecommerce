﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerce.Communication.Models.Requests.User
{
    public class RequestChangePhoneNumberCodeRequest : RequestBase
    {
        public string PhoneNumber { get; set; }
        public string Name { get; set; }
    }
}
