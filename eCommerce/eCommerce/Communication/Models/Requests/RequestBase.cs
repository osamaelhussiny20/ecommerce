﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace eCommerce.Communication.Models.Requests
{
    public class RequestBase
    {
        public string RuntimePlatform { get { return Device.RuntimePlatform; } }
    }
}
