﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerce.Communication.Models.Responses
{
    public class ResponseBase
    {
        public string Message { get; set; }
        public bool Successfull { get; set; }
    }
}
