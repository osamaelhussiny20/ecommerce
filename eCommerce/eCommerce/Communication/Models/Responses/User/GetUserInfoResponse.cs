﻿using System;
using System.Collections.Generic;
using System.Text;
using eCommerce.Enums;
using eCommerce.Models.User;

namespace eCommerce.Communication.Models.Responses.User
{
    public class GetUserInfoResponse : ResponseBase
    {
      public UserInfo userInfo { get; set; }
    }

        public class ImagePath
    {
        public string Path { get; set; }
        public Guid ID { get; set; }
    }


}
