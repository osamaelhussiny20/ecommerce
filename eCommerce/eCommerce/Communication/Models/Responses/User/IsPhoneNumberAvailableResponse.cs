﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerce.Communication.Models.Responses.User
{
    public class IsPhoneNumberAvailableResponse : ResponseBase
    {
        public bool IsAvailable { get; set; }
    }
}
