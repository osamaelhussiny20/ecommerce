﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerce.Communication.Models.Responses.User
{
    public class RequestChangePhoneNumberCodeResponse : ResponseBase
    {
        public string Code { get; set; }
    }
}
