﻿using eCommerce.Enums;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace eCommerce.Controls
{
    public class CarouselIndicator : Grid
    {
        private readonly StackLayout _indicator = new StackLayout() { Orientation = StackOrientation.Horizontal, HorizontalOptions = LayoutOptions.CenterAndExpand };

        private ImageSource _unselectedImageSource;
        private ImageSource _selectedImageSource;

        public CarouselIndicator()
        {
            HorizontalOptions = LayoutOptions.CenterAndExpand;
            RowDefinitions.Add(new RowDefinition() { Height = GridLength.Auto });
            Children.Add(_indicator);
        }

        public static readonly BindableProperty PositionProperty = BindableProperty.Create(nameof(Position), typeof(int), typeof(CarouselIndicator), 0, BindingMode.TwoWay, propertyChanging: PositionChanging);
        public static readonly BindableProperty ItemsSourceProperty = BindableProperty.Create(nameof(ItemsSource), typeof(IEnumerable), typeof(CarouselIndicator), Enumerable.Empty<object>(), BindingMode.OneWay, propertyChanged: ItemsChanged);
        public static readonly BindableProperty SelectedIndicatorProperty = BindableProperty.Create(nameof(SelectedIndicator), typeof(string), typeof(CarouselIndicator), string.Empty, BindingMode.OneWay);
        public static readonly BindableProperty UnselectedIndicatorProperty = BindableProperty.Create(nameof(UnselectedIndicator), typeof(string), typeof(CarouselIndicator), string.Empty, BindingMode.OneWay);
        public static readonly BindableProperty IndicatorWidthProperty = BindableProperty.Create(nameof(IndicatorWidth), typeof(double), typeof(CarouselIndicator), 0.0, BindingMode.OneWay);
        public static readonly BindableProperty IndicatorHeightProperty = BindableProperty.Create(nameof(IndicatorHeight), typeof(double), typeof(CarouselIndicator), 0.0, BindingMode.OneWay);

        public string SelectedIndicator
        {
            get { return (string)GetValue(SelectedIndicatorProperty); }
            set { SetValue(SelectedIndicatorProperty, value); }
        }

        public string UnselectedIndicator
        {
            get { return (string)GetValue(UnselectedIndicatorProperty); }
            set { SetValue(UnselectedIndicatorProperty, value); }
        }

        public double IndicatorWidth
        {
            get { return (double)GetValue(IndicatorWidthProperty); }
            set { SetValue(IndicatorWidthProperty, value); }
        }

        public double IndicatorHeight
        {
            get { return (double)GetValue(IndicatorHeightProperty); }
            set { SetValue(IndicatorHeightProperty, value); }
        }

        public int Position
        {
            get { return (int)GetValue(PositionProperty); }
            set { SetValue(PositionProperty, value); }
        }

        public IEnumerable ItemsSource
        {
            get { return (IEnumerable)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        private void Clear()
        {
            _indicator.Children.Clear();
        }

        private void Init(int position)
        {
            if (_unselectedImageSource == null)
            {
                _unselectedImageSource = ImageSource.FromFile(UnselectedIndicator);
            }

            if (_selectedImageSource == null)
            {
                _selectedImageSource = ImageSource.FromFile(SelectedIndicator);
            }

            if (_indicator.Children.Count > 0)
            {
                for (int indicatorIndex = 0; indicatorIndex < _indicator.Children.Count; indicatorIndex++)
                {
                    if (((Image)_indicator.Children[indicatorIndex]).ClassId == nameof(CarouselIndicatorState.Selected) && indicatorIndex != position)
                    {
                        _indicator.Children[indicatorIndex] = BuildImage(CarouselIndicatorState.Unselected, indicatorIndex);
                    }
                    else if (((Image)_indicator.Children[indicatorIndex]).ClassId == nameof(CarouselIndicatorState.Unselected) && indicatorIndex == position)
                    {
                        _indicator.Children[indicatorIndex] = BuildImage(CarouselIndicatorState.Selected, indicatorIndex);
                    }
                }
            }
            else
            {
                var items = ItemsSource.GetEnumerator();
                int indicatorCount = 0;

                while (items.MoveNext())
                {
                    Image image = null;

                    if (position == indicatorCount)
                    {
                        image = BuildImage(CarouselIndicatorState.Selected, indicatorCount);
                    }
                    else
                    {
                        image = BuildImage(CarouselIndicatorState.Unselected, indicatorCount);
                    }

                    _indicator.Children.Add(image);

                    indicatorCount++;
                }
            }
        }

        private Image BuildImage(CarouselIndicatorState state, int position)
        {
            var image = new Image()
            {
                WidthRequest = IndicatorWidth,
                HeightRequest = IndicatorHeight,
                ClassId = state.ToString()
            };

            switch (state)
            {
                case CarouselIndicatorState.Selected:
                    image.Source = _selectedImageSource;
                    break;
                case CarouselIndicatorState.Unselected:
                    image.Source = _unselectedImageSource;
                    break;
                default:
                    throw new Exception("Invalid state selected");
            }

            image.GestureRecognizers.Add(new TapGestureRecognizer() { Command = new Command(() => { Position = position; }) });

            return image;
        }

        private static void PositionChanging(object bindable, object oldValue, object newValue)
        {
            var carouselIndicator = bindable as CarouselIndicator;

            carouselIndicator.Init(Convert.ToInt32(newValue));
        }

        private static void ItemsChanged(object bindable, object oldValue, object newValue)
        {
            var carouselIndicator = bindable as CarouselIndicator;

            carouselIndicator.Clear();
            carouselIndicator.Init(0);
        }
    }

}
