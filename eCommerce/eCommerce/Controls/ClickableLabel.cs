﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace eCommerce.Controls
{
    public class ClickableLabel : Label
    {
        public static BindableProperty CommandProperty =
            BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(WriteReviewView));

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public void OnPressed()
        {
            Opacity = 0.3;
        }

        public void OnReleased()
        {
            Opacity = 1;

            if (Command != null && Command.CanExecute(null))
            {
                Command.Execute(null);
            }
        }

        public void OnMovedOutside()
        {
            Opacity = 1;
        }
    }

}
