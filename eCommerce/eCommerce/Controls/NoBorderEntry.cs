﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace eCommerce.Controls
{
    public class NoBorderEntry : Entry
    {
        public static BindableProperty DisplaySuggestionsProperty = BindableProperty.Create(nameof(DisplaySuggestions), typeof(bool), typeof(NoBorderEntry), true);
        public static BindableProperty FloatingButtonProperty = BindableProperty.Create(nameof(FloatingButton), typeof(NoBorderButton), typeof(NoBorderEntry));
        public NoBorderEntry() { }

        public bool DisplaySuggestions
        {
            get { return (bool)GetValue(DisplaySuggestionsProperty); }
            set { SetValue(DisplaySuggestionsProperty, value); }
        }
        public NoBorderButton FloatingButton
        {
            get { return (NoBorderButton)GetValue(FloatingButtonProperty); }
            set { SetValue(FloatingButtonProperty, value); }
        }
    }
}
