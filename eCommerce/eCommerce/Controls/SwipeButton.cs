﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace eCommerce.Controls
{
    public class SwipeButton : ContentView
    {
        public static readonly BindableProperty TextProperty =
            BindableProperty.Create(nameof(Text), typeof(string), typeof(SwipeButton), string.Empty);

        public static readonly BindableProperty DescriptionProperty =
            BindableProperty.Create(nameof(Description), typeof(string), typeof(SwipeButton), string.Empty);

        public static readonly BindableProperty IconSourceProperty =
            BindableProperty.Create(nameof(IconSource), typeof(string), typeof(SwipeButton), string.Empty);

        public static readonly BindableProperty DragButtonSizeProperty =
            BindableProperty.Create(nameof(DragButtonSize), typeof(int), typeof(SwipeButton), 0);

        public static readonly BindableProperty DragButtonBackgroundColorProperty =
            BindableProperty.Create(nameof(DragButtonBackgroundColor), typeof(Color), typeof(SwipeButton), Color.Default);

        public static readonly BindableProperty TextColorProperty =
            BindableProperty.Create(nameof(TextColor), typeof(Color), typeof(SwipeButton), Color.Default);

        public static readonly BindableProperty FontFamilyProperty =
            BindableProperty.Create(nameof(FontFamily), typeof(string), typeof(SwipeButton), string.Empty);

        public static readonly BindableProperty FontSizeProperty =
            BindableProperty.Create(nameof(FontSize), typeof(float), typeof(SwipeButton), 14.0f);

        public static readonly BindableProperty DescriptionFontSizeProperty =
            BindableProperty.Create(nameof(DescriptionFontSize), typeof(float), typeof(SwipeButton), 12.0f);

        public static readonly BindableProperty SwipeComplitedCommandProperty =
            BindableProperty.Create(nameof(SwipeComplitedCommand), typeof(ICommand), typeof(SwipeButton));

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public string Description
        {
            get { return (string)GetValue(DescriptionProperty); }
            set { SetValue(DescriptionProperty, value); }
        }

        public string IconSource
        {
            get { return (string)GetValue(IconSourceProperty); }
            set { SetValue(IconSourceProperty, value); }
        }

        public int DragButtonSize
        {
            get { return (int)GetValue(DragButtonSizeProperty); }
            set { SetValue(DragButtonSizeProperty, value); }
        }

        public Color DragButtonBackgroundColor
        {
            get { return (Color)GetValue(DragButtonBackgroundColorProperty); }
            set { SetValue(DragButtonBackgroundColorProperty, value); }
        }

        public Color TextColor
        {
            get { return (Color)GetValue(TextColorProperty); }
            set { SetValue(TextColorProperty, value); }
        }

        public string FontFamily
        {
            get { return (string)GetValue(FontFamilyProperty); }
            set { SetValue(FontFamilyProperty, value); }
        }

        public float FontSize
        {
            get { return (float)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        public float DescriptionFontSize
        {
            get { return (float)GetValue(DescriptionFontSizeProperty); }
            set { SetValue(DescriptionFontSizeProperty, value); }
        }

        public ICommand SwipeComplitedCommand
        {
            get { return (ICommand)GetValue(SwipeComplitedCommandProperty); }
            set { SetValue(SwipeComplitedCommandProperty, value); }
        }
    }

}
