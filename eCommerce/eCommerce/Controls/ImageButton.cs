﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace eCommerce.Controls
{
    public class ImageButton : Image
    {
        public static BindableProperty CommandProperty =
            BindableProperty.Create(nameof(Command), typeof(ICommand), typeof(ImageButton));

        public ICommand Command
        {
            get { return (ICommand)GetValue(CommandProperty); }
            set { SetValue(CommandProperty, value); }
        }

        public void OnButtonPressed()
        {
            Opacity = 0.3;
        }

        public void OnButtonReleased()
        {
            Opacity = 1;

            if (Command != null && Command.CanExecute(null))
            {
                Command.Execute(null);
            }
        }

        public void OnMovedOutside()
        {
            Opacity = 1;
        }
    }

}
