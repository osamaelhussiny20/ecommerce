﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace eCommerce.Controls
{
    public class Wheel : ContentView
    {
        public static BindableProperty SpinAmountProperty = BindableProperty.Create(nameof(SpinAmount), typeof(int), typeof(Wheel), 0, BindingMode.TwoWay);
        public static BindableProperty ProfilePictureProperty = BindableProperty.Create(nameof(ProfilePicture), typeof(byte[]), typeof(Wheel));
        public static BindableProperty BackgroundImageProperty = BindableProperty.Create(nameof(BackgroundImage), typeof(string), typeof(Wheel));

        public int SpinAmount
        {
            get { return (int)GetValue(SpinAmountProperty); }
            set { SetValue(SpinAmountProperty, value); }
        }

        public byte[] ProfilePicture
        {
            get { return (byte[])GetValue(ProfilePictureProperty); }
            set { SetValue(ProfilePictureProperty, value); }
        }

        public string BackgroundImage
        {
            get { return (string)GetValue(BackgroundImageProperty); }
            set { SetValue(BackgroundImageProperty, value); }
        }
    }

}
