﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace eCommerce.Controls
{
    public class CarouselItemView : ContentView
    {
        public static BindableProperty ParentBindingContextProperty = BindableProperty.Create(nameof(ParentBindingContext), typeof(object), typeof(CarouselItemView), null);

        public object ParentBindingContext
        {
            get { return GetValue(ParentBindingContextProperty); }
            set { SetValue(ParentBindingContextProperty, value); }
        }
    }
}
