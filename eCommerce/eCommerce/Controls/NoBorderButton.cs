﻿using eCommerce.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace eCommerce.Controls
{
    public class NoBorderButton : Button
    {
        public static BindableProperty DisplayAboveKeyboardProperty = BindableProperty.Create(nameof(DisplayAboveKeyboard), typeof(bool), typeof(NoBorderButton), false);
        public static BindableProperty TextAligmentProperty = BindableProperty.Create(nameof(TextAligment), typeof(ButtonTextAlignment), typeof(NoBorderButton), ButtonTextAlignment.Default);
        public static readonly BindableProperty IsMultilineProperty = BindableProperty.Create(nameof(IsMultiline), typeof(bool), typeof(NoBorderButton), false);

        public NoBorderButton() { }

        public bool DisplayAboveKeyboard
        {
            get { return (bool)GetValue(DisplayAboveKeyboardProperty); }
            set { SetValue(DisplayAboveKeyboardProperty, value); }
        }

        public ButtonTextAlignment TextAligment
        {
            get { return (ButtonTextAlignment)GetValue(TextAligmentProperty); }
            set { SetValue(TextAligmentProperty, value); }
        }

        public bool IsMultiline
        {
            get { return (bool)GetValue(IsMultilineProperty); }
            set { SetValue(IsMultilineProperty, value); }
        }
    }

}
