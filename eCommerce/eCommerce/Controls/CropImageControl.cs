﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace eCommerce.Controls
{
    public class CropImageControl : ContentView
    {
        public static readonly BindableProperty ImageSourceProperty =
            BindableProperty.Create(nameof(ImageSource),
                typeof(string), typeof(CropImageControl),
                string.Empty, propertyChanged: OnImageSourcePropertyChanged);

        public static readonly BindableProperty ImageSourceAddedCommandProperty =
            BindableProperty.Create(nameof(ImageSourceAddedCommand),
                typeof(ICommand),
                typeof(CropImageControl));

        public static readonly BindableProperty ImageCroppedCommandProperty =
            BindableProperty.Create(nameof(ImageCroppedCommand),
                typeof(ICommand),
                typeof(CropImageControl));

        public static readonly BindableProperty GetCroppedImageCommandProperty =
            BindableProperty.Create(nameof(GetCroppedImageCommand),
                typeof(ICommand),
                typeof(CropImageControl),
                defaultBindingMode: BindingMode.TwoWay,
                propertyChanged: GetCroppedImageCommandPropertyChanged);

        public CropImageControl()
        {
            //BindingContext = this;
        }

        private static void GetCroppedImageCommandPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var obj = bindable as CropImageControl;
            var param = newValue as ICommand;
            if (obj != null && param != null)
            {
                obj.GetCroppedImageCommand = param;
            }
        }

        public string ImageSource
        {
            get { return (string)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }

        public ICommand ImageSourceAddedCommand
        {
            get { return (ICommand)GetValue(ImageSourceAddedCommandProperty); }
            set { SetValue(ImageSourceAddedCommandProperty, value); }
        }

        public ICommand ImageCroppedCommand
        {
            get { return (ICommand)GetValue(ImageCroppedCommandProperty); }
            set { SetValue(ImageCroppedCommandProperty, value); }
        }

        public ICommand GetCroppedImageCommand
        {
            get { return (ICommand)GetValue(GetCroppedImageCommandProperty); }
            set { SetValue(GetCroppedImageCommandProperty, value); }
        }

        private static void OnImageSourcePropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var obj = bindable as CropImageControl;
            var imageSource = newValue as string;
            if (obj != null && imageSource != null)
            {
                obj.ImageSource = imageSource;
                obj.ImageSourceAddedCommand?.Execute(imageSource);
            }
        }
    }
}
