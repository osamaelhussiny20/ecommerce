﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace eCommerce.Models
{
    public class PageTypeGroup : ObservableCollection<GroupedViewModel>
    {
        public string Title { get; set; }
        public string ShortName { get; set; }
        public PageTypeGroup(string title, string shortName)
        {
            Title = title;
            ShortName = shortName;
        }

        public static IList<PageTypeGroup> All { private set; get; }
    }

    public class GroupedViewModel
    {
        public string ViewModelTitle { get; set; }
        public Type ViewModelType { get; set; }
        public Action ActionHandler { get; set; }
        public string ViewModelDetail { get; set; }
        public bool ShowAsModal { get; set; }

        public GroupedViewModel(string viewModelTitle, string viewModelDetail = "", Type viewModelType = null, Action actionHandler = null, bool showAsModal = false)
        {
            this.ViewModelTitle = viewModelTitle;
            this.ViewModelType = viewModelType;
            this.ActionHandler = actionHandler;
            this.ViewModelDetail = viewModelDetail;
            this.ShowAsModal = showAsModal;
        }
    }
}
