﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerce.Models.Country
{
    public class DefaultCountryModel : CountryModel
    {
        public DefaultCountryModel()
        {
            Name = "Denmark";
            TwoLetterISORegionName = "DK";
            PhoneExtension = "+45";
            CurrencySymbol = "DKK";
            PhoneNumberLenght = 8;
        }
    }
}
