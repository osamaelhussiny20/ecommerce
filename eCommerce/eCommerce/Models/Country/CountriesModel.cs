﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace eCommerce.Models.Country
{
    public class CountriesModel
    {
        [JsonProperty("countries")]
        public List<CountryModel> Countries { get; set; }
    }
}
