﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace eCommerce.Models.Country
{
    public class CountryModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("twoLetterISORegionName")]
        public string TwoLetterISORegionName { get; set; }
        [JsonProperty("phoneExtension")]
        public string PhoneExtension { get; set; }
        [JsonProperty("currencySymbol")]
        public string CurrencySymbol { get; set; }
        [JsonProperty("phoneNumberLenght")]
        public int PhoneNumberLenght { get; set; }
    }
}
