﻿using System;
using System.Collections.Generic;
using System.Text;
using eCommerce.Communication.Models.Responses.User;

namespace eCommerce.Models.User
{
    public class ActiveUser
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }

        public ActiveUser() { }
        public ActiveUser(GetUserInfoResponse responseModel)
        {
            Id = responseModel.userInfo.id;
            FirstName = responseModel.userInfo.firstName;
            LastName = responseModel.userInfo.lastName;
            PhoneNumber = responseModel.userInfo.phoneNumber;
            Email = responseModel.userInfo.email;
        }
    }
}
