﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerce.Models.User
{
   public class UserInfo
    {
        public string profileImage { get; set; }
        //public Lazyloader lazyLoader { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string phoneNumber { get; set; }
        public string email { get; set; }
        public string activationCode { get; set; }
        public string passCode { get; set; }
        public bool isBlocked { get; set; }
        public bool hasAwaitingInvitation { get; set; }
        public bool isNew { get; set; }
        public DateTime joinedDate { get; set; }
        public string deviceID { get; set; }
        public object runtimePlatform { get; set; }
        public bool phoneNumberConfirmed { get; set; }
        public string id { get; set; }
    }
}
