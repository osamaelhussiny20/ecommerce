﻿using System;
using System.Collections.Generic;
using System.Text;
using eCommerce.Enums;
using eCommerce.Models.Country;
using eCommerce.Models.User;
using Newtonsoft.Json;
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace eCommerce
{
    public static class GeneralSettings
    {
        private static ISettings AppSettings => CrossSettings.Current;

        #region Setting Constants

        private const string ActiveUserFirstNameKey = "UserFirstName";
        private const string ActiveUserLastNameKey = "UserLastName";
        private const string ActiveUserIdKey = "UserId";
        private const string ActiveUserPhoneNumberKey = "UserPhoneNumber";
        private const string ActiveUserProfilePictureKey = "UserProfilePicture";
        private const string CanFastLoginKey = "CanFastLogin";
        private const string SelectedCountry = "SelectedCountry";
        private const string ActiveUserEmailKey = "Email";
        private const string CompanyIdKey = "Company";
        private const string TransactionIdKey = "TransactionId";
        private const string AppUserTypeKey = "AppUserType";
        private const string HasCompanyKey = "HasCompany";
      
        private const string NumberOfCompanyUnConfirmedKey = "Numberofunconfirmed";
        private const string NumberOfCompanyConfirmedKey = "Numberofconfirmed";
        private static readonly string DefaultStringValue = string.Empty;
        private static readonly int DefaultIntValue = 0;
        private static readonly bool DefaultBooleanValue = false;

        #endregion

        public static void SetActiveUserInfoToDefaults()
        {
            CanFastLogin = DefaultBooleanValue;
            ActiveUserEmail = DefaultStringValue;
            ActiveUserFirstName = DefaultStringValue;
            ActiveUserLastName = DefaultStringValue;
            ActiveUserId = DefaultStringValue;
            ActiveUserType = 0;
        }
        public static ActiveUser GetActiveUser()
        {
            return new ActiveUser
            {
                FirstName = ActiveUserFirstName,
                LastName = ActiveUserLastName,
                Id = ActiveUserId,
                PhoneNumber = ActiveUserPhoneNumber,
                Email = ActiveUserEmail,
            };
        }

        public static void UpdateActiveUser(ActiveUser user)
        {
            ActiveUserFirstName = user.FirstName;
            ActiveUserLastName = user.LastName;
            ActiveUserPhoneNumber = user.PhoneNumber;
            ActiveUserId = user.Id;
            ActiveUserEmail = user.Email;
        }
        public static int ActiveUserType
        {
            get => AppSettings.GetValueOrDefault(AppUserTypeKey, DefaultIntValue);
            set => AppSettings.AddOrUpdateValue(AppUserTypeKey, value);
        }
        public static string ActiveUserEmail
        {
            get => AppSettings.GetValueOrDefault(ActiveUserEmailKey, DefaultStringValue);
            set => AppSettings.AddOrUpdateValue(ActiveUserEmailKey, value);
        }

        public static AppUserType ActiveUserAppUserType
        {
            get => ((AppUserType)ActiveUserType);
            set => ActiveUserType = ((int)value);
        }

        public static string ActiveFullName
        {
            get
            {
                return string.Format("{0} {1}", ActiveUserFirstName, ActiveUserLastName);
            }
        }

        public static string ActiveUserFirstName
        {
            get => AppSettings.GetValueOrDefault(ActiveUserFirstNameKey, DefaultStringValue);
            set => AppSettings.AddOrUpdateValue(ActiveUserFirstNameKey, value);
        }

        public static string ActiveUserLastName
        {
            get => AppSettings.GetValueOrDefault(ActiveUserLastNameKey, DefaultStringValue);
            set => AppSettings.AddOrUpdateValue(ActiveUserLastNameKey, value);
        }

        public static string ActiveUserId
        {
            get => AppSettings.GetValueOrDefault(ActiveUserIdKey, DefaultStringValue);
            set => AppSettings.AddOrUpdateValue(ActiveUserIdKey, value);
        }
        public static string ActiveUserProfilePicture
        {
            get => AppSettings.GetValueOrDefault(ActiveUserProfilePictureKey, DefaultStringValue);
            set => AppSettings.AddOrUpdateValue(ActiveUserProfilePictureKey, value);
        }
        public static string ActiveUserPhoneNumber
        {
            get => AppSettings.GetValueOrDefault(ActiveUserPhoneNumberKey, DefaultStringValue);
            set => AppSettings.AddOrUpdateValue(ActiveUserPhoneNumberKey, value);
        }

        public static string CompanyId
        {
            get => AppSettings.GetValueOrDefault(CompanyIdKey, DefaultStringValue);
            set => AppSettings.AddOrUpdateValue(CompanyIdKey, value);
        }
        public static bool HasCompanies
        {
            get => AppSettings.GetValueOrDefault(HasCompanyKey, DefaultBooleanValue);
            set => AppSettings.AddOrUpdateValue(HasCompanyKey, value);
        }
        public static string NumberofUnConfirmedCompanies
        {
            get => AppSettings.GetValueOrDefault(NumberOfCompanyUnConfirmedKey, DefaultStringValue);
            set => AppSettings.AddOrUpdateValue(NumberOfCompanyUnConfirmedKey, value);
        }
        public static string NumberofConfirmedCompanies
        {
            get => AppSettings.GetValueOrDefault(NumberOfCompanyConfirmedKey, DefaultStringValue);
            set => AppSettings.AddOrUpdateValue(NumberOfCompanyConfirmedKey, value);
        }
        public static string TransactionId
        {
            get => AppSettings.GetValueOrDefault(TransactionIdKey, DefaultStringValue);
            set => AppSettings.AddOrUpdateValue(TransactionIdKey, value);

        }

        public static bool CanFastLogin
        {
            get => AppSettings.GetValueOrDefault(CanFastLoginKey, DefaultBooleanValue);
            set => AppSettings.AddOrUpdateValue(CanFastLoginKey, value);
        }

        public static string SelectedCountryJson
        {
            get => AppSettings.GetValueOrDefault(SelectedCountry, JsonConvert.SerializeObject(new DefaultCountryModel()));
            set => AppSettings.AddOrUpdateValue(SelectedCountry, value);
        }

    }
}
