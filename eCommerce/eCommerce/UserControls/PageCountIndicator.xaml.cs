﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace eCommerce.UserControls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PageCounIndicator : Grid
    {
        public static readonly BindableProperty CurrentPageNumberProperty =
            BindableProperty.Create(nameof(CurrentPageNumber), typeof(string), typeof(PageCounIndicator), string.Empty);
        public static readonly BindableProperty TotalPageNumberProperty =
            BindableProperty.Create(nameof(TotalPageNumber), typeof(string), typeof(PageCounIndicator), string.Empty);

        public PageCounIndicator()
        {
            InitializeComponent();
        }

        public string CurrentPageNumber
        {
            get { return (string)GetValue(CurrentPageNumberProperty); }
            set { SetValue(CurrentPageNumberProperty, value); }
        }

        public string TotalPageNumber
        {
            get { return (string)GetValue(TotalPageNumberProperty); }
            set { SetValue(TotalPageNumberProperty, value); }
        }
    }
}