﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace eCommerce.UserControls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RoundedPanel
    {
        public static BindableProperty HeaderTextProperty = BindableProperty.Create(nameof(HeaderText), typeof(string), typeof(RoundedPanel), string.Empty);
        public static BindableProperty OptionTextProperty = BindableProperty.Create(nameof(OptionText), typeof(string), typeof(RoundedPanel), string.Empty);
        public static BindableProperty HeaderFontSizeProperty = BindableProperty.Create(nameof(HeaderFontSize), typeof(double), typeof(RoundedPanel), 14.0);
        public static BindableProperty OptionFontSizeProperty = BindableProperty.Create(nameof(OptionFontSize), typeof(double), typeof(RoundedPanel), 12.0);
        public static BindableProperty IconSourceProperty = BindableProperty.Create(nameof(IconSource), typeof(string), typeof(RoundedPanel), string.Empty);

        public RoundedPanel()
        {
            InitializeComponent();
        }

        public string HeaderText
        {
            get { return (string)GetValue(HeaderTextProperty); }
            set { SetValue(HeaderTextProperty, value); }
        }

        public string OptionText
        {
            get { return (string)GetValue(OptionTextProperty); }
            set { SetValue(OptionTextProperty, value); }
        }

        public double HeaderFontSize
        {
            get { return (double)GetValue(HeaderFontSizeProperty); }
            set { SetValue(HeaderFontSizeProperty, value); }
        }

        public double OptionFontSize
        {
            get { return (double)GetValue(OptionFontSizeProperty); }
            set { SetValue(OptionFontSizeProperty, value); }
        }

        public string IconSource
        {
            get { return (string)GetValue(IconSourceProperty); }
            set { SetValue(IconSourceProperty, value); }
        }
    }
}