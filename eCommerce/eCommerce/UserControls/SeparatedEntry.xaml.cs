﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace eCommerce.UserControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SeparatedEntry : Grid
    {
        public static readonly BindableProperty TextProperty =
            BindableProperty.Create(nameof(Text), typeof(string), typeof(SeparatedEntry), string.Empty, BindingMode.TwoWay, propertyChanged: OnTextChanged);

        public static readonly BindableProperty MaxLenghtProperty =
            BindableProperty.Create(nameof(MaxLenght), typeof(int), typeof(SeparatedEntry), 6, BindingMode.TwoWay);

        public static readonly BindableProperty FontSizeProperty =
           BindableProperty.Create(nameof(FontSize), typeof(int), typeof(SeparatedEntry), 32, BindingMode.TwoWay);

        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                if (value != null)
                {
                    SetValue(TextProperty, value);
                }
            }
        }

        public int MaxLenght
        {
            get
            {
                return (int)GetValue(MaxLenghtProperty);
            }
            set
            {
                SetValue(MaxLenghtProperty, value);
            }
        }

        public int FontSize
        {
            get
            {
                return (int)GetValue(FontSizeProperty);
            }
            set
            {
                SetValue(FontSizeProperty, value);
            }
        }

        private int CurrentTextLenght { get; set; }
        private List<Label> Entries { get; set; }
        private List<BoxView> Deviders { get; set; }

        public SeparatedEntry()
        {
            InitializeComponent();
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            BuildUi();
        }

        private void BuildUi()
        {
            Entries = new List<Label>();
            Deviders = new List<BoxView>();
            ColumnSpacing = 10;
            RowSpacing = 0;
            for (int i = 0; i < MaxLenght; i++)
            {
                ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });
                Entries.Add(new Label
                {
                    FontSize = FontSize,
                    TextColor = Color.White,
                    BackgroundColor = Color.Transparent,
                    Margin = new Thickness(0),
                    HorizontalTextAlignment = TextAlignment.Center,
                    VerticalTextAlignment = TextAlignment.End,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                });

                Deviders.Add(new BoxView
                {
                    HeightRequest = 1,
                    BackgroundColor = Color.White,
                    HorizontalOptions = LayoutOptions.Fill,
                    VerticalOptions = LayoutOptions.End
                });

                Children.Add(Entries[i], i, 0);
                Children.Add(Deviders[i], i, 0);
                Children.Add(new BoxView { BackgroundColor = Color.Transparent }, 0, 0);

                SetColumnSpan(Children.LastOrDefault(), ColumnDefinitions.Count);
            }
        }

        private void UpdateUi(string inputString)
        {
            if (inputString.Length > MaxLenght)
                return;

            int newTexLenght = inputString.Length;

            if (newTexLenght > CurrentTextLenght)
            {
                for (int i = CurrentTextLenght; i <= newTexLenght - 1; i++)
                {
                    Entries[i].Text = inputString.Substring(i, 1);
                    Deviders[i].IsVisible = false;
                }
            }
            else if (newTexLenght < CurrentTextLenght)
            {
                for (int i = CurrentTextLenght - 1; i > newTexLenght - 1; i--)
                {
                    Entries[i].Text = string.Empty;
                    Deviders[i].IsVisible = true;
                }
            }

            CurrentTextLenght = newTexLenght;
        }

        private static void OnTextChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var obj = (SeparatedEntry)bindable;
            if (newValue != null)
            {
                obj.UpdateUi((string)newValue);
            }
        }
    }
}