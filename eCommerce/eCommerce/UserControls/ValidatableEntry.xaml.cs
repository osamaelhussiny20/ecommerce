﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace eCommerce.UserControls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ValidatableEntry : ContentView
    {
		public ValidatableEntry ()
		{
			InitializeComponent ();
		}
        public static readonly BindableProperty TextProperty =
          BindableProperty.Create(nameof(Text), typeof(string), typeof(ValidatableEntry), string.Empty, BindingMode.TwoWay, propertyChanged: OnTextChanged);

        public static readonly BindableProperty PlaceholderProperty =
            BindableProperty.Create(nameof(Placeholder), typeof(string), typeof(ValidatableEntry), string.Empty, BindingMode.TwoWay, propertyChanged: OnPlaceholderChaaged);

        public static readonly BindableProperty FontSizeProperty =
            BindableProperty.Create(nameof(FontSize), typeof(int), typeof(ValidatableEntry), 16, BindingMode.TwoWay);

        public static readonly BindableProperty IsValidProperty =
            BindableProperty.Create(nameof(IsValid), typeof(bool), typeof(ValidatableEntry), defaultValue: true, propertyChanged: OnValidPropertyChanged);

        public static readonly BindableProperty ValidationCommandProperty =
          BindableProperty.Create(nameof(ValidationCommand), typeof(ICommand), typeof(ValidatableEntry));

        public static readonly BindableProperty KeyboardTypeProperty =
            BindableProperty.Create(nameof(KeyboardType), typeof(Keyboard), typeof(ValidatableEntry), Keyboard.Text, BindingMode.TwoWay);

        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                if (value != null)
                {
                    SetValue(TextProperty, value);
                }
            }
        }

        public string Placeholder
        {
            get
            {
                return (string)GetValue(PlaceholderProperty);
            }
            set
            {
                if (value != null)
                {
                    SetValue(PlaceholderProperty, value);
                    TextEntry.Placeholder = value;
                }
            }
        }

        public int FontSize
        {
            get
            {
                return (int)GetValue(FontSizeProperty);
            }
            set
            {
                SetValue(FontSizeProperty, value);
            }
        }

        public bool IsValid
        {
            get
            {
                return (bool)GetValue(IsValidProperty);
            }
            set
            {
                SetValue(IsValidProperty, value);
            }
        }

        public Command ValidationCommand
        {
            get
            {
                return (Command)GetValue(ValidationCommandProperty);
            }
            set
            {
                SetValue(ValidationCommandProperty, value);
            }
        }

        public Keyboard KeyboardType
        {
            get
            {
                return (Keyboard)GetValue(KeyboardTypeProperty);
            }
            set
            {
                SetValue(KeyboardTypeProperty, value);
            }
        }

      
        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            ValidationCommand = new Command(ValidationCommandExecute);
        }

        private void ValidationCommandExecute(object obj)
        {
            if (obj is bool)
            {
                ValidIndicator.IsVisible = (bool)obj;
                InValidIndicator.IsVisible = !(bool)obj;
            }
        }

        public void ResetValidationState()
        {
            InValidIndicator.IsVisible = false;
            ValidIndicator.IsVisible = false;
        }

        private static void OnTextChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var currentControl = bindable as ValidatableEntry;

            if (currentControl != null)
            {
                currentControl.ResetValidationState();
            }
        }

        private static void OnPlaceholderChaaged(BindableObject bindable, object oldValue, object newValue)
        {
            var currentControl = bindable as ValidatableEntry;

            if (currentControl != null)
            {
                currentControl.Placeholder = (string)newValue;
                currentControl.TextEntry.Placeholder = (string)newValue;
            }
        }

        private static void OnValidPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            //var currentControl = bindable as ValidatableEntry;

            //if (currentControl != null)
            //{
            //    currentControl.ValidIndicator.IsVisible = (bool)newValue;
            //    currentControl.InValidIndicator.IsVisible = !(bool)newValue;
            //}
        }
    }
}
