﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace eCommerce.UserControls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class RatingView : Grid, IDisposable
    {
        private int _filledItemIndex;
        private int _filledStaticRatingIndex;
        private static bool _shouldSetStaticRating;
        private TapGestureRecognizer _tapGestureRecognizer;

        public static readonly BindableProperty RatingPinCountProperty =
            BindableProperty.Create(nameof(RatingPinCount), typeof(int), typeof(RatingView), 4, BindingMode.TwoWay);

        public static readonly BindableProperty FillIconSourceProperty =
            BindableProperty.Create(nameof(FillIconSource), typeof(string), typeof(RatingView), string.Empty, BindingMode.TwoWay);

        public static readonly BindableProperty EmptyIconSourceProperty =
            BindableProperty.Create(nameof(EmptyIconSource), typeof(string), typeof(RatingView), string.Empty, BindingMode.TwoWay);

        public static readonly BindableProperty StaticRatingProperty =
            BindableProperty.Create(nameof(StaticRating), typeof(int), typeof(RatingView), -1, propertyChanged: StaticRatingChanged);

        public static readonly BindableProperty RatingProperty =
            BindableProperty.Create(nameof(Rating), typeof(int), typeof(RatingView), 0, BindingMode.TwoWay);

        public int StaticRating
        {
            get { return (int)GetValue(StaticRatingProperty); }
            set { SetValue(StaticRatingProperty, value); }
        }

        public int Rating
        {
            get { return (int)GetValue(RatingProperty); }
            set { SetValue(RatingProperty, value); }
        }

        public RatingView()
        {
            InitializeComponent();

            _tapGestureRecognizer = new TapGestureRecognizer
            {
                NumberOfTapsRequired = 1
            };

            _tapGestureRecognizer.Tapped += TapGesture_Tapped;
        }

        public string EmptyIconSource
        {
            get { return (string)GetValue(EmptyIconSourceProperty); }
            set
            {
                if (value != null)
                {
                    SetValue(EmptyIconSourceProperty, value);
                }
            }
        }

        public string FillIconSource
        {
            get { return (string)GetValue(FillIconSourceProperty); }
            set
            {
                if (value != null)
                {
                    SetValue(FillIconSourceProperty, value);
                }
            }
        }

        public int RatingPinCount
        {
            get { return (int)GetValue(RatingPinCountProperty); }
            set { SetValue(RatingPinCountProperty, value); }
        }

        private List<Image> FillRatingList { get; set; }
        private List<Image> EmptyRatingList { get; set; }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            BuildControl();

            if (_shouldSetStaticRating)
            {
                SetStaticRating();
            }
        }

        private void BuildControl()
        {
            FillRatingList = new List<Image>();
            EmptyRatingList = new List<Image>();

            for (int index = 0; index < RatingPinCount; index++)
            {
                ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });

                var emptyImage = new Image
                {
                    Source = EmptyIconSource,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    Margin = new Thickness(0, 0)
                };

                var fillImage = new Image
                {
                    Source = FillIconSource,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    Margin = new Thickness(0, 0),
                    Opacity = 0.1f
                };

                fillImage.GestureRecognizers.Add(_tapGestureRecognizer);

                EmptyRatingList.Add(emptyImage);
                FillRatingList.Add(fillImage);

                Children.Add(EmptyRatingList[index], index, 0);
                Children.Add(FillRatingList[index], index, 0);
            }
        }

        private void TapGesture_Tapped(object sender, EventArgs e)
        {
            var selectedImage = sender as Image;
            var selectedItemIndex = FillRatingList.FindIndex(image => image == selectedImage);

            Rating = selectedItemIndex + 1;

            UpdateRating(selectedItemIndex);
        }

        private void UpdateRating(int selectedItemIndex)
        {
            for (int index = 0; index <= selectedItemIndex; index++)
            {
                FillRatingList[index].Opacity = 1;
            }

            if (_filledItemIndex > selectedItemIndex)
            {
                for (int index = _filledItemIndex; index > selectedItemIndex; index--)
                {
                    FillRatingList[index].Opacity = 0.1f;
                }
            }

            _filledItemIndex = selectedItemIndex;
        }

        private void SetStaticRating()
        {
            if (StaticRating > RatingPinCount)
            {
                throw new ArgumentException($"Rating should consist of maximum {RatingPinCount} pins");
            }

            for (int index = 0; index < StaticRating; index++)
            {
                FillRatingList[index].Opacity = 1;
            }

            if (_filledStaticRatingIndex > StaticRating)
            {
                for (int index = _filledStaticRatingIndex - 1; index >= StaticRating; index--)
                {
                    FillRatingList[index].Opacity = 0;
                }
            }

            _filledStaticRatingIndex = StaticRating;
        }

        private static void StaticRatingChanged(BindableObject bindable, object oldValue, object newValue)
        {
            if (bindable is RatingView ratingView)
            {
                ratingView.InputTransparent = true;

                if (ratingView.FillRatingList != null)
                {
                    ratingView.SetStaticRating();
                }
                else
                {
                    _shouldSetStaticRating = true;
                }
            }
        }

        public void Dispose()
        {
            _tapGestureRecognizer.Tapped -= TapGesture_Tapped;
        }
    }
}