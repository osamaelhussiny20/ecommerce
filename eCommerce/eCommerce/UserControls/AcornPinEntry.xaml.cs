﻿using FFImageLoading.Svg.Forms;
using Plugin.Vibrate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace eCommerce.UserControls
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AcornPinEntry : Grid
    {
        public static readonly BindableProperty PinProperty =
            BindableProperty.Create(nameof(Pin), typeof(string), typeof(AcornPinEntry), string.Empty, propertyChanged: OnPinChanged);

        public static readonly BindableProperty PinLenghtProperty =
            BindableProperty.Create(nameof(PinLenght), typeof(int), typeof(AcornPinEntry), 4, BindingMode.TwoWay, propertyChanged: OnPinLenghtChanged);

        public static readonly BindableProperty FillIconSourceProperty =
            BindableProperty.Create(nameof(FillIconSource), typeof(string), typeof(AcornPinEntry), string.Empty, BindingMode.TwoWay, propertyChanged: FillIconSourceChanged);

        public static readonly BindableProperty EmptyIconSourceProperty =
            BindableProperty.Create(nameof(EmptyIconSource), typeof(string), typeof(AcornPinEntry), string.Empty, BindingMode.TwoWay, propertyChanged: EmptyIconSourceChanged);

        public static readonly BindableProperty IsValidProperty =
            BindableProperty.Create(nameof(IsValid), typeof(bool), typeof(AcornPinEntry), true);

        public string EmptyIconSource
        {
            get
            {
                return (string)GetValue(EmptyIconSourceProperty);
            }
            set
            {
                if (value != null)
                {
                    SetValue(EmptyIconSourceProperty, value);
                }
            }
        }

        public string FillIconSource
        {
            get
            {
                return (string)GetValue(FillIconSourceProperty);
            }
            set
            {
                if (value != null)
                {
                    SetValue(FillIconSourceProperty, value);
                }
            }
        }

        public int PinLenght
        {
            get
            {
                return (int)GetValue(PinLenghtProperty);
            }
            set
            {
                SetValue(PinLenghtProperty, value);
            }
        }

        public string Pin
        {
            get
            {
                return (string)GetValue(PinProperty);
            }
            set
            {
                if (value != null)
                {
                    SetValue(PinProperty, value);
                    UpdateUi(value);
                }
            }
        }

        public bool IsValid
        {
            get
            {
                return (bool)GetValue(IsValidProperty);
            }
            set
            {
                SetValue(IsValidProperty, value);
            }
        }

        //private List<Image> FillIconsList { get; set; }
        //private List<Image> EmptyIconsList { get; set; }
        private List<SvgCachedImage> EmptySVGIconsList { get; set; }
        private List<SvgCachedImage> FillSVGIconsList { get; set; }

        private int CurrentPinLenght { get; set; }

        public AcornPinEntry()
        {
            InitializeComponent();
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            BuildUI();
        }

        public void BuildUI()
        {
            //FillIconsList = new List<Image>();
            //EmptyIconsList = new List<Image>();
            EmptySVGIconsList = new List<SvgCachedImage>();
            FillSVGIconsList = new List<SvgCachedImage>();

            for (int i = 0; i < PinLenght; i++)
            {
                ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Auto });
                EmptySVGIconsList.Add(new SvgCachedImage
                {
                    Source = EmptyIconSource,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    Margin = new Thickness(0, 0),
                });
                FillSVGIconsList.Add(new SvgCachedImage
                {
                    Source = FillIconSource,
                    VerticalOptions = LayoutOptions.FillAndExpand,
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    IsVisible = false,
                    Margin = new Thickness(0, 0),
                });
                //EmptyIconsList.Add(new Image { Source = EmptyIconSource, VerticalOptions = LayoutOptions.FillAndExpand, HorizontalOptions = LayoutOptions.FillAndExpand, Margin = new Thickness(0, 0) });
                //FillIconsList.Add(new Image { Source = FillIconSource, VerticalOptions = LayoutOptions.FillAndExpand, HorizontalOptions = LayoutOptions.FillAndExpand, IsVisible = false, Margin = new Thickness(0, 0) });
                Children.Add(EmptySVGIconsList[i], i, 0);
                Children.Add(FillSVGIconsList[i], i, 0);
            }
        }

        private void UpdateUi(string pinString)
        {
            if (pinString.Length > PinLenght)
                return;

            int newPinLenght = pinString.Length;

            if (newPinLenght > CurrentPinLenght)
            {
                for (int i = CurrentPinLenght; i <= newPinLenght - 1; i++)
                {
                    FillSVGIconsList[i].IsVisible = true;
                }
            }
            else if (newPinLenght < CurrentPinLenght)
            {
                for (int i = CurrentPinLenght - 1; i > newPinLenght - 1; i--)
                {
                    FillSVGIconsList[i].IsVisible = false;
                }
            }

            CurrentPinLenght = newPinLenght;
        }

        static void OnPinChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var obj = (AcornPinEntry)bindable;
            if (newValue != null)
            {
                if (obj.CurrentPinLenght == obj.PinLenght && obj.IsValid == false)
                {
                    ShakeView(obj);
                    Vibrate();
                }

                obj.UpdateUi((string)newValue);
            }
        }

        private static void Vibrate()
        {// i added time span here
            if (CrossVibrate.Current.CanVibrate)
            {
                CrossVibrate.Current.Vibration(TimeSpan.FromSeconds(0.5));
            }
        }

        private static void ShakeView(AcornPinEntry pin)
        {
            var repeatCount = 0;
            var shakeAnimation = new Animation
                    {
                        { 0, 0.5, new Animation(translation => pin.TranslationX = translation, 0, 10, Easing.CubicOut, null) },
                        { 0.5, 1, new Animation(translation => pin.TranslationX = translation, 10, 0, Easing.CubicOut, null) },
                        { 0, 0.5, new Animation(translation => pin.TranslationX = translation, 0, -10, Easing.CubicOut, null) },
                        { 0.5, 1, new Animation(translation => pin.TranslationX = translation, -10, 0, Easing.CubicOut, null) }
                    };
            shakeAnimation.Commit(pin, "Shake", 10, 250, null, null, () =>
            {
                repeatCount++;
                return repeatCount <= 2 ? true : false;
            });
        }

        private static void OnPinLenghtChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var obj = (AcornPinEntry)bindable;
            obj.PinLenght = (int)newValue;
        }


        private static void EmptyIconSourceChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var obj = (AcornPinEntry)bindable;
            obj.EmptyIconSource = (string)newValue;
        }

        private static void FillIconSourceChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var obj = (AcornPinEntry)bindable;
            obj.FillIconSource = (string)newValue;
        }
    }
}