﻿using BaseMvvmToolkit.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Plugin.Vibrate;

namespace eCommerce.UserControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RectDigitalKeyboard : ContentView
    {
        public static readonly BindableProperty TextProperty =
            BindableProperty.Create(nameof(Text), typeof(string), typeof(RectDigitalKeyboard), string.Empty, BindingMode.TwoWay);

        public static readonly BindableProperty RightButtonTextProperty =
            BindableProperty.Create(nameof(Text), typeof(string), typeof(RectDigitalKeyboard), string.Empty, BindingMode.TwoWay);

        public static readonly BindableProperty MaxLenghtProperty =
           BindableProperty.Create(nameof(MaxLenght), typeof(int), typeof(RectDigitalKeyboard), 6, BindingMode.TwoWay);

        public static readonly BindableProperty RightButtonCommandProperty =
           BindableProperty.Create(nameof(RightButtonCommand), typeof(ICommand), typeof(RectDigitalKeyboard));

        public static readonly BindableProperty EnterCompletedCommandProperty =
           BindableProperty.Create(nameof(EnterCompletedCommand), typeof(ICommand), typeof(RectDigitalKeyboard));

        public static readonly BindableProperty TextEnteredCommandProperty =
            BindableProperty.Create(nameof(TextEnteredCommandProperty), typeof(ICommand), typeof(RectDigitalKeyboard));
        public static readonly BindableProperty DeleteCommandProperty =
            BindableProperty.Create(nameof(DeleteCommand), typeof(ICommand), typeof(RectDigitalKeyboard));

        public BaseCommand DeleteCommand
        {
            get
            {
                return (BaseCommand)GetValue(DeleteCommandProperty);
            }
            set
            {
                SetValue(DeleteCommandProperty, value);
            }
        }

        //TODO: Make generic command
        public BaseCommand RightButtonCommand
        {
            get
            {
                return (BaseCommand)GetValue(RightButtonCommandProperty);
            }
            set
            {
                SetValue(RightButtonCommandProperty, value);
            }
        }
        public BaseCommand TextEnteredCommand
        {
            get
            {
                return (BaseCommand)GetValue(TextEnteredCommandProperty);
            }
            set
            {
                SetValue(TextEnteredCommandProperty, value);
            }
        }
        public BaseCommand EnterCompletedCommand
        {
            get
            {
                return (BaseCommand)GetValue(EnterCompletedCommandProperty);
            }
            set
            {
                SetValue(EnterCompletedCommandProperty, value);
            }
        }

        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                if (value != null)
                    SetValue(TextProperty, value);
            }
        }

        public string RightButtonText
        {
            get
            {
                return (string)GetValue(RightButtonTextProperty);
            }
            set
            {
                if (value != null)
                    SetValue(RightButtonTextProperty, value);
            }
        }

        public int MaxLenght
        {
            get
            {
                return (int)GetValue(MaxLenghtProperty);
            }
            set
            {
                SetValue(MaxLenghtProperty, value);
            }
        }

        public RectDigitalKeyboard()
        {
            InitializeComponent();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {// i chnaged it from 10 to 5
            if (CrossVibrate.Current.CanVibrate)
                CrossVibrate.Current.Vibration(new TimeSpan(0, 0, 0, 0, 5));

            var button = sender as Button;
            if (button != null)
            {
                if (Text == null)
                {
                    Text = string.Empty;
                }

                var value = button.Text;
                int number;
                if (int.TryParse(value, out number))
                {
                    if (Text.Length >= MaxLenght)
                    {
                        return;
                    }

                    Text += number;
                }
            }
            if (Text.Length >= MaxLenght)
            {
                if (EnterCompletedCommand != null && EnterCompletedCommand.CanExecute(null))
                    await EnterCompletedCommand.ExecuteAsync(sender);
            }
            else
            {
                if (TextEnteredCommand != null && TextEnteredCommand.CanExecute(null))
                    await TextEnteredCommand.ExecuteAsync(Text);
            }
        }

        private async void DeleteButtonClicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Text))
            {
                Text = Text.Remove(Text.Length - 1, 1);
                if (TextEnteredCommand != null && TextEnteredCommand.CanExecute(null))
                    await TextEnteredCommand.ExecuteAsync(Text);
            }
        }

        private async void RightButtonClicked(object sender, EventArgs e)
        {
            if (RightButtonCommand != null && RightButtonCommand.CanExecute(null))
                await RightButtonCommand.ExecuteAsync(sender);
        }
    }
}
