﻿using BaseMvvmToolkit.Commands;
using Plugin.Vibrate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace eCommerce.UserControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CircleDigitalKeyboard : ContentView
    {
        public static readonly BindableProperty TextProperty =
            BindableProperty.Create(nameof(Text), typeof(string), typeof(CircleDigitalKeyboard), string.Empty, BindingMode.TwoWay);

        public static readonly BindableProperty MaxLenghtProperty =
            BindableProperty.Create(nameof(MaxLenght), typeof(int), typeof(CircleDigitalKeyboard), 4, BindingMode.TwoWay);

        public static readonly BindableProperty LeftButtonCommandProperty =
            BindableProperty.Create(nameof(LeftButtonCommand), typeof(ICommand), typeof(CircleDigitalKeyboard));

        public static readonly BindableProperty EnterCompletedCommandProperty =
            BindableProperty.Create(nameof(EnterCompletedCommand), typeof(ICommand), typeof(CircleDigitalKeyboard));
        //TODO: Make generic command
        public BaseCommand LeftButtonCommand
        {
            get
            {
                return (BaseCommand)GetValue(LeftButtonCommandProperty);
            }
            set
            {
                SetValue(LeftButtonCommandProperty, value);
            }
        }

        public BaseCommand EnterCompletedCommand
        {
            get
            {
                return (BaseCommand)GetValue(EnterCompletedCommandProperty);
            }
            set
            {
                SetValue(EnterCompletedCommandProperty, value);
            }
        }

        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                if (value != null)
                    SetValue(TextProperty, value);
            }
        }

        public int MaxLenght
        {
            get
            {
                return (int)GetValue(MaxLenghtProperty);
            }
            set
            {
                SetValue(MaxLenghtProperty, value);
            }
        }

        public CircleDigitalKeyboard()
        {
            InitializeComponent();
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            // i changed it from 10 to 5
            if (CrossVibrate.Current.CanVibrate)
                CrossVibrate.Current.Vibration(new TimeSpan(0, 0, 0, 0, 5));

            var button = sender as Button;
            if (button != null)
            {
                if (Text == null)
                {
                    Text = string.Empty;
                }

                var value = button.Text;
                int number;
                if (int.TryParse(value, out number))
                {
                    if (Text.Length >= MaxLenght)
                    {
                        return;
                    }

                    Text += number;
                }
            }

            if (Text.Length >= MaxLenght)
            {
                if (EnterCompletedCommand != null && EnterCompletedCommand.CanExecute(null))
                    await EnterCompletedCommand.ExecuteAsync(sender);
            }
        }

        private async void LeftButtonClicked(object sender, EventArgs e)
        {
            if (LeftButtonCommand != null && LeftButtonCommand.CanExecute(null))
                await LeftButtonCommand.ExecuteAsync(sender);
        }

        private void DeleteButtonClicked(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Text))
            {
                Text = Text.Remove(Text.Length - 1, 1);
            }
        }
    }
}