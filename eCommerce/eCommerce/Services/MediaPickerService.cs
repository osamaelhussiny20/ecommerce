﻿using CloudStorageManager;
using eCommerce.Constants;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Services
{
    public class MediaPickerService : IMediaPickerService
    {
        private CloudStorageService _cloudStorageManager;

        public MediaPickerService()
        {
            _cloudStorageManager = new CloudStorageService(AppConstants.CloudStorageConnectionstring);
        }
        public async Task<string> TakePictureFromLibrary(bool uploadToCloud)
        {
            var mediaFile = await GetMediaFileFromLibrary();
            return await GetURL(uploadToCloud, mediaFile);
        }

        private async Task<string> GetURL(bool uploadToCloud, MediaFile mediaFile)
        {
            if (uploadToCloud)
            {
                var urlPath = await _cloudStorageManager.UploadImageToCloud(new Guid(), mediaFile.GetStream());
                return urlPath;
            }
            else
            {
                return mediaFile.Path;
            }
        }

        public async Task<string> TakePictureFromCamera(bool uploadToCloud = false)
        {
            var mediaFile = await GetMediaFileFromCamera();
            return await GetURL(uploadToCloud, mediaFile);
        }
        private async Task<MediaFile> GetMediaFileFromLibrary()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                throw new NotSupportedException("This device is not supporting photo picking");
            }

            var file = await CrossMedia.Current.PickPhotoAsync();

            if (file == null)
            {
                throw new ArgumentException("File not found");
            }
            return file;
        }

        private async Task<MediaFile> GetMediaFileFromCamera()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                throw new NotSupportedException("This device's camera is not available");
            }

            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                Directory = "eCommerceImages",
                Name = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds().ToString(),
            });

            if (file == null)
            {
                throw new ArgumentException("File not found");
            }

            return file;
        }
    }
}
