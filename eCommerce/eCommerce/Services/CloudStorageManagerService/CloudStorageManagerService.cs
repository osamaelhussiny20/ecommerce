﻿using CloudStorageManager;
using eCommerce.Constants;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Services.CloudStorageManagerService
{
    public class CloudStorageManagerService : ICloudStorageManagerService
    {
        private CloudStorageService _cloudStorageManager;

        public CloudStorageManagerService()
        {
            _cloudStorageManager = new CloudStorageService(AppConstants.CloudStorageConnectionstring);
        }

        public async Task<string> UploadImage(Guid id, byte[] image)
        {
            using (var stream = new MemoryStream(image))
            {
                return await _cloudStorageManager.UploadImageToCloud(id, stream);
            }
        }
    }
}