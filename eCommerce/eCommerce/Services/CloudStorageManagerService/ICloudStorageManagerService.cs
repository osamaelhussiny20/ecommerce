﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Services.CloudStorageManagerService
{
    public interface ICloudStorageManagerService
    {
        Task<string> UploadImage(Guid id, byte[] image);
    }
}
