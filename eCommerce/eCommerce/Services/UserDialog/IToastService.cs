﻿using eCommerce.Enums;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace eCommerce.Services.UserDialog
{
    public interface IToastService
    {
        void ShowToast(string message, ToastType toastType, int duration = 5, int fontSize = 14, Color? backgroundColor = null, Color? textColor = null);
        void DismissNotification(int duration = 5);
    }
}
