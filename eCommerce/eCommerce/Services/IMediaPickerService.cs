﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Services
{
    public interface IMediaPickerService
    {
        Task<string> TakePictureFromLibrary(bool uploadToCloud = false);
        Task<string> TakePictureFromCamera(bool uploadToCloud = false);

    }
}