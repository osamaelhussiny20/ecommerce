﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerce.Services
{
    public enum DeviceOrientations
    {
        Undefined,
        Landscape,
        Portrait
    }
    public interface IDeviceOrientation
    {
        DeviceOrientations GetOrientation();
        void SetLandscape();
        void SetPortrait();
    }
}
