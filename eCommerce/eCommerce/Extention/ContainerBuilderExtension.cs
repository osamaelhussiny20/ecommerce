﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using eCommerce.Constants;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Refit;

namespace eCommerce.Extention
{
    public static class ContainerBuilderExtension
    {
        public static ContainerBuilder RegisterRestApi<T>(this ContainerBuilder builder) where T : class
        {
            var jsonSerializerSettings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver(),
                DateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind,
                DateFormatHandling = DateFormatHandling.IsoDateFormat,
                NullValueHandling = NullValueHandling.Ignore
            };
            jsonSerializerSettings.Converters.Add(new StringEnumConverter());

            builder.Register(c => RestService.For<T>(AppConstants.BaseServerEndpoint2, new RefitSettings
            {
                JsonSerializerSettings = jsonSerializerSettings
            })).As<T>();

            return builder;
        }
    }
}
