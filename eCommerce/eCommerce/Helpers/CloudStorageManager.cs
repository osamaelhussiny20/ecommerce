﻿using System;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace eCommerce.Helpers
{
    public enum CloudImageType
    {
        ProfilePicture,
        CompanyProfilePicture,
    }

    public enum CloudAppDataFiles
    {
        CountriesJson,

    }
    public class CloudStorageManager
    {
        const string PROFILE_IMAGE_KEY = "PROFILE";
        const string COMPANY_IMAGE_KEY = "COMPANY";
        const string DEFAULT_IMAGE_KEY = "DEFAULT";
        const string CloudStoragePath = "DefaultEndpointsProtocol=https;AccountName=mypallet;AccountKey=T4r9sQucmgQ+oDQke29rNt/HXBA6611f8e4tmX/aUxDaFgOP823KWv/6YRCeDrzG++dqUhehV+KUv/UFdCNlIA==;EndpointSuffix=core.windows.net";
        // TODO there is an issure in cloud storage account 
        private static CloudBlobContainer GetContainer(Guid userID)
        {
            var account = CloudStorageAccount.Parse(CloudStoragePath);
            var client = account.CreateCloudBlobClient();

            var container = client.GetContainerReference(userID.ToString());

            return container;
        }
        private static CloudBlobContainer GetContainer(string containerName)
        {
            var account = CloudStorageAccount.Parse(CloudStoragePath);
            var client = account.CreateCloudBlobClient();

            var container = client.GetContainerReference(containerName);

            return container;
        }

        public static async Task<string> UploadImage(Guid id, CloudImageType type, Stream image)
        {
            var container = GetContainer(id);

            await container.CreateIfNotExistsAsync();

            var imageName = DEFAULT_IMAGE_KEY;

            switch (type)
            {
                case CloudImageType.ProfilePicture:
                    imageName = PROFILE_IMAGE_KEY;
                    break;
                case CloudImageType.CompanyProfilePicture:
                    imageName = COMPANY_IMAGE_KEY;
                    break;
                default:
                    imageName = DEFAULT_IMAGE_KEY;
                    break;
            }

            var imageBlob = container.GetBlockBlobReference(imageName);
            await imageBlob.UploadFromStreamAsync(image);

            return imageName;
        }

        public static async Task<byte[]> GetImage(Guid userID, CloudImageType type)
        {
            var container = GetContainer(userID);

            var imageName = DEFAULT_IMAGE_KEY;

            switch (type)
            {
                case CloudImageType.ProfilePicture:
                    imageName = PROFILE_IMAGE_KEY;
                    break;
                case CloudImageType.CompanyProfilePicture:
                    imageName = COMPANY_IMAGE_KEY;
                    break;
                default:
                    imageName = DEFAULT_IMAGE_KEY;
                    break;
            }

            var blob = container.GetBlobReference(imageName);

            if (await blob.ExistsAsync())
            {
                await blob.FetchAttributesAsync();
                byte[] blobBytes = new byte[blob.Properties.Length];
                await blob.DownloadToByteArrayAsync(blobBytes, 0);
                return blobBytes;
            }

            return null;
        }
        public static async Task GetAppDataFile(CloudAppDataFiles cloudAppDataFiles)
        {
            var container = GetContainer("appdata");

            var fileName = "";

            switch (cloudAppDataFiles)
            {
                case CloudAppDataFiles.CountriesJson:
                    fileName = "countries.json";
                    var blob = container.GetBlockBlobReference(fileName);

                    if (await blob.ExistsAsync())
                    {
                        string text;
                        using (var memoryStream = new MemoryStream())
                        {
                            await blob.DownloadToStreamAsync(memoryStream);
                            text = System.Text.Encoding.UTF8.GetString(memoryStream.ToArray());
                        }

                        var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
                        var filePath = Path.Combine(documentsPath, "Countries.json");
                        System.IO.File.WriteAllText(filePath, text);
                    }
                    break;
            }
        }
    }
}
