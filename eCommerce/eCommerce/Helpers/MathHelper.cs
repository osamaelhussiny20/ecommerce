﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eCommerce.Helpers
{
    public static class MathHelper
    {
        public static double RadianToDegree(double radians)
        {
            return radians * 180 / Math.PI;
        }
    }
}
