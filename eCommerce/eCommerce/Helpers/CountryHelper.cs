﻿using System;
using System.Collections.Generic;
using System.Text;
using eCommerce.Models.Country;
using Newtonsoft.Json;

namespace eCommerce.Helpers
{
    public static class CountryHelper
    {
        public static CountryModel GetCurrentCountry()
        {
            return JsonConvert.DeserializeObject<CountryModel>(GeneralSettings.SelectedCountryJson);
        }
        public static void SetCurrentCountry(CountryModel country)
        {
            GeneralSettings.SelectedCountryJson = JsonConvert.SerializeObject(country);
        }
    }
}
